#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: arguments
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午10:42
#==================================

import numpy as np
import ipdb
import argparse
import utils
import os
import tensorflow as tf
# adding breakpoint with ipdb.set_trace()

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

@utils.Pipe
def parse_arguments(*args):
    parser = argparse.ArgumentParser(description='training dataset')
    parser.add_argument('-log_path',dest='log_path',type = str,default="./log/",help='log_path')
    parser.add_argument('-model',dest='model',type = str,default="basic_lstm",help='model name')
    parser.add_argument('-saved_model_path',dest='saved_model_path',type=str,default='./saved_model',help="saved_model_path")
    parser.add_argument('-task',dest='task',type=str,default="train",help='task')
    parser.add_argument('-GPU_index',dest='GPU_index',type=str,default="0",help="the deploying GPU id")
    parser.add_argument('-DATA_PATH',dest='DATA_PATH',type=str,default="./",help="data_path")
    parser.add_argument('-CATE_PATH',dest='CATE_PATH',type=str,default="./",help="category data path")
    parser.add_argument('-BATCH',dest='BATCH',type=int,default=128,help="batch size")
    parser.add_argument("-USER_NUMBER",dest='USER_NUMBER',type=int,default=32266,help="user number")
    parser.add_argument("-ITEM_NUMBER",dest='ITEM_NUMBER',type=int,default=23812,help="item number")
    parser.add_argument("-CATE_NUMBER",dest='CATE_NUMBER',type=int,default=2010,help="item number")
    parser.add_argument("-learning_rate",dest='learning_rate',type=float,default=1.0,help="the learning rate")
    parser.add_argument("-CELL_TYPE",dest='CELL_TYPE',type=str,default="gru",help="gru")
    parser.add_argument("-window_size",dest='window_size',type=str,default="[3]",help="cnn window size")
    parser.add_argument("-restore_model",dest='restore_model',type=str2bool,default="False",help="restore_model")
    parser.add_argument("-test_user_num",dest='test_user_num',type=int,default=3000,help='test_user_number')
    parser.add_argument("-max_update",dest='max_update',type=int,default=100000,help='max_update')
    parser.add_argument("-max_cycle",dest='max_cycle',type=float,default=200.0,help='max_cycle')
    parser.add_argument("-LATENT_FACTOR",dest='LATENT_FACTOR',type=int,default=200,help='latent_factor')
    parser.add_argument("-RNN_HIDDEN",dest='RNN_HIDDEN',type=int,default=200,help='RNN_HIDDEN')
    parser.add_argument("-regularizer",dest='regularizer',type=float,default=0.01,help='RNN_HIDDEN')
    parser.add_argument("-optimizer_name",dest='optimizer_name',type=str,default="adagrad",help='the optimizer for training')
    args = parser.parse_args()
    args.GPU_OPTION = tf.GPUOptions(allow_growth=True)
    args.RANDOM_SEED = 123
    args.window_size = eval(args.window_size)
    args.RNN_LAYER = 1
    return args

@utils.Pipe
def initialize(config,*args):
    if not os.path.isdir(config.log_path):
        os.mkdir(config.log_path)
    utils.log.set_log_path(os.path.join(config.log_path,config.model)+"_"+str(config.task)+"_"+str(utils.get_now_time())+".log")
    utils.log.info('saving log file in '+utils.log().log_path)
    utils.log.structure_info("config for experiments",list(vars(config).items()))
    return config