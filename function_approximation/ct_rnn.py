#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: continuous_rnn
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/17,上午10:00
#==================================

import numpy as np
import ipdb
from .rnn_user import rnn_user
import tensorflow as tf

class ct_rnn(rnn_user):
    def _create_placeholders(self):
        self.uid = tf.placeholder(tf.int32,(None,))
        self.purchased_items = tf.placeholder(tf.int32,(None,None,1))
        self.purchased_time_gap = tf.placeholder(tf.float32,(None,None,1))
        self.target_index = tf.placeholder(tf.int32,(None,2))
        self.positve_sample = tf.placeholder(tf.int32,(None,))
        self.negative_sample = tf.placeholder(tf.int32,(None,))
        self.labels = tf.placeholder(tf.float32,(None,))


    def _update_placehoders(self):
        self.placeholders["all"] = {"uid":self.uid,
                                    "purchased_items":self.purchased_items,
                                    "target_index":self.target_index,
                                    "purchased_time_gap":self.purchased_time_gap,
                                    "positive_sample":self.positve_sample,
                                    "negative_sample":self.negative_sample,
                                    "labels":self.labels}
        predicts = ["uid","purchased_items","purchased_time_gap","target_index","positive_sample"]
        predicts_all = ["uid","purchased_items","purchased_time_gap","target_index"]
        self.placeholders["predict"]={item:self.placeholders["all"][item] for item in predicts}
        self.placeholders["optimizer"] = self.placeholders["all"]
        self.placeholders["predict_all"] = {item:self.placeholders["all"][item] for item in predicts_all}


    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        purchased_items = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     purchased_items,initial_state)
        context = tf.concat([tf.gather_nd(self.rnn_outputs, self.target_index),u_embedding])
        weight = tf.Variable(tf.random_uniform((self.config.LATENT_FACTOR + self.config.RNN_HIDDEN+self.config.RNN_HIDDEN+1,1),
                                                    -1.0, 1.0), name='last_layer', trainable=True)
        pfeature = tf.concat([context,tf.nn.embedding_lookup(item_feature,self.positve_sample), tf.ones((tf.shape(context)[0], 1))], 1)
        nfeature = tf.concat([context,tf.nn.embedding_lookup(item_feature,self.negative_sample), tf.ones((tf.shape(context)[0], 1))], 1)
        self.pscore = tf.matmul(pfeature,weight)
        self.nscore = tf.matmul(nfeature,weight)
        # score function
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        all_feature = tf.concat([expand_hidden,item_feature,tf.ones((tf.shape(expand_hidden)[0], 1))],1)
        self.all_score = tf.matmul(all_feature,weight)

    def _create_optimizer(self):
        sample_score = self.pscore-self.nscore
        self.loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_score,labels=self.labels))
        optimizer = tf.train.AdamOptimizer(self.config.learning_rate)
        gvs = optimizer.compute_gradients(self.loss)
        self.gvs = gvs
        capped_gvs = [(tf.clip_by_value(grad, -0.5, 0.5), var) for grad, var in gvs]
        self.optimizer = optimizer.apply_gradients(capped_gvs,global_step=self.global_step)

    def optimize_model(self,sess,data):
        feed_dicts = self._get_feed_dict("optimizer",data)
        return sess.run([self.loss,self.optimizer],feed_dicts)[0]

    def predict(self,sess,data):
        feed_dicts = self._get_feed_dict("predict", data)
        return sess.run(self.pscore, feed_dicts)

    def predict_all_score(self,sess,data):
        # for predict all the data must be one dimension
        feed_dicts = self._get_feed_dict("predict_all", data)
        return sess.run(self.all_score, feed_dicts)
