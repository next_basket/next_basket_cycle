#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: rnn_cnn
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/17,下午1:45
#==================================

import numpy as np
import ipdb
from .rnn_user import *
import tensorflow as tf
import prettytensor as pt

class rnn_cnn(rnn_user):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        self.purchased = tf.transpose(tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2),[1,0,2])
        #### cnn
        window_size = self.config.window_size
        in_channels = self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,int(self.config.LATENT_FACTOR/len(window_size))),0,0.1),trainable=True))
            bias.append(tf.Variable(np.zeros((int(self.config.LATENT_FACTOR/len(window_size)),)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.add(tf.nn.conv1d(value=self.purchased, filters=filters[-1], stride=1, padding='SAME'),bias[-1]))
            valid_cnn_output.append(tf.transpose(cnn_outputs[-1],perm=[1, 0, 2])) # shape: [batch_size,max_len,local_out_channels]
        self.valid_cnn_output = valid_cnn_output
        valid_cnn_output = tf.concat(valid_cnn_output,2)
        print(valid_cnn_output)
        if self.config.CELL_TYPE=="ctlstm":
            valid_cnn_output = tf.concat([valid_cnn_output,self.purchased_time],2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     valid_cnn_output,initial_state)
        context = tf.concat([tf.gather_nd(self.rnn_outputs, self.target_index),u_embedding],1)
        # print(context)
        context = tf.layers.dense(context, 2*self.config.LATENT_FACTOR,
                        kernel_initializer=initializer,
                        bias_initializer=tf.zeros_initializer,
                        trainable=self.trainable, activation=tf.nn.tanh)
        # last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,2*self.config.LATENT_FACTOR+
        #                                             len(window_size)*int(self.config.LATENT_FACTOR/(len(window_size)+1))),-1.0, 1.0), name='last_layer',trainable=True)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER, 2*self.config.LATENT_FACTOR), -1.0, 1.0), name='last_layer', trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer,self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer,self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context,pfeature),axis=1),(-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context,nfeature),axis=1),(-1,))
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden,last_layer),1)

class rnn_cnn_new(rnn_cnn):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        self.purchased = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        self.rnn_outputs, rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                      initializer,
                                                      self.config.RNN_HIDDEN,
                                                      self.purchased, initial_state)
        rnn_outputs = tf.transpose(self.rnn_outputs,[1,0,2])
        #### cnn
        window_size = [1,3,5,10]
        in_channels = self.config.LATENT_FACTOR
        out_channels = self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,out_channels),0,0.1),trainable=True))
            bias.append(tf.Variable(np.zeros((out_channels,)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.add(tf.nn.conv1d(value=rnn_outputs, filters=filters[-1], stride=1, padding='SAME'),bias[-1]))
            valid_cnn_output.append(cnn_outputs[-1]) # shape: [batch_size,max_len,local_out_channels]
        self.valid_cnn_output = valid_cnn_output
        valid_cnn_output = tf.concat(valid_cnn_output,2)
        print(valid_cnn_output)
        context = tf.concat([tf.gather_nd(tf.transpose(valid_cnn_output,[1,0,2]),self.target_index),u_embedding],1)
        # print(context)
        # context = tf.layers.dense(context, 2*self.config.LATENT_FACTOR,
        #                 kernel_initializer=initializer,
        #                 bias_initializer=tf.zeros_initializer,
        #                 trainable=self.trainable, activation=tf.nn.tanh)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,5*self.config.LATENT_FACTOR),-1.0, 1.0), name='last_layer',trainable=True)
        # last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER, 2*self.config.LATENT_FACTOR), -1.0, 1.0), name='last_layer', trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer,self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer,self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context,pfeature),axis=1),(-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context,nfeature),axis=1),(-1,))
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden,last_layer),1)


class rnn_cnn_dot_product(rnn_cnn):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        self.purchased = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        self.rnn_outputs, rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                      initializer,
                                                      self.config.RNN_HIDDEN,
                                                      self.purchased, initial_state)
        rnn_outputs = tf.transpose(self.rnn_outputs,[1,0,2])
        #### cnn
        window_size = [1,3,5,10]
        in_channels = self.config.LATENT_FACTOR
        out_channels = self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,out_channels),0,0.1),trainable=True))
            bias.append(tf.Variable(np.zeros((out_channels,)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.add(tf.nn.conv1d(value=rnn_outputs, filters=filters[-1], stride=1, padding='SAME'),bias[-1])) # shape: [batch_size,max_len,local_out_channels]
            valid_cnn_output.append(cnn_outputs[-1])
        for i,item in enumerate(valid_cnn_output):
            print(item)
            valid_cnn_output[i] = tf.multiply(tf.gather_nd(tf.transpose(item,[1,0,2]),self.target_index),u_embedding)
        self.valid_cnn_output = valid_cnn_output
        context = tf.concat(valid_cnn_output,1)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,4*self.config.LATENT_FACTOR),-1.0, 1.0), name='last_layer',trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer,self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer,self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context,pfeature),axis=1),(-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context,nfeature),axis=1),(-1,))
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden,last_layer),1)
    pass


class rnn_concat_cnn(rnn_cnn):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        self.purchased = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        self.rnn_outputs, rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                      initializer,
                                                      self.config.RNN_HIDDEN,
                                                      self.purchased, initial_state)
        rnn_outputs = tf.transpose(self.rnn_outputs,[1,0,2])
        user_feature4rnn = tf.reshape(tf.tile(u_embedding, [1,tf.shape(rnn_outputs)[1]]), (tf.shape(rnn_outputs)[0], -1, tf.shape(rnn_outputs)[2]))
        rnn_outputs = tf.concat([user_feature4rnn,rnn_outputs],2)
        #### cnn
        window_size = [1,3,5,10]
        in_channels = 2*self.config.LATENT_FACTOR
        out_channels = 2*self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,int(out_channels/len(window_size))),0,0.1),trainable=True))
            bias.append(tf.Variable(np.zeros(int((out_channels/len(window_size)),)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.add(tf.nn.conv1d(value=rnn_outputs, filters=filters[-1], stride=1, padding='SAME'),bias[-1])) # shape: [batch_size,max_len,local_out_channels]
            valid_cnn_output.append(tf.gather_nd(tf.transpose(cnn_outputs[-1],[1,0,2]),self.target_index))
        context = tf.concat(valid_cnn_output,1)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,out_channels),-1.0, 1.0), name='last_layer',trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer,self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer,self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context,pfeature),axis=1),(-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context,nfeature),axis=1),(-1,))
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden,last_layer),1)
    pass


class rnn_dot_product(rnn_cnn):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        self.purchased = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        self.rnn_outputs, rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                      initializer,
                                                      self.config.RNN_HIDDEN,
                                                      self.purchased, initial_state)
        rnn_outputs = tf.transpose(self.rnn_outputs,[1,0,2])
        user_feature4rnn = tf.reshape(tf.tile(u_embedding, [1,tf.shape(rnn_outputs)[1]]), (tf.shape(rnn_outputs)[0], -1, tf.shape(rnn_outputs)[2]))
        rnn_outputs = tf.multiply(rnn_outputs,user_feature4rnn)

        #### cnn
        window_size = [1,3,5,10]
        in_channels = self.config.LATENT_FACTOR
        out_channels = self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,int(out_channels/len(window_size))),0,0.1),trainable=True))
            bias.append(tf.Variable(np.zeros((int(out_channels/len(window_size)),)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.add(tf.nn.conv1d(value=rnn_outputs, filters=filters[-1], stride=1, padding='SAME'),bias[-1])) # shape: [batch_size,max_len,local_out_channels]
            valid_cnn_output.append(tf.gather_nd(tf.transpose(cnn_outputs[-1],[1,0,2]),self.target_index))
        context = tf.concat(valid_cnn_output,1)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,out_channels),-1.0, 1.0), name='last_layer',trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer,self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer,self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context,pfeature),axis=1),(-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context,nfeature),axis=1),(-1,))
        expand_hidden = tf.reshape(tf.tile(context,[1,self.config.ITEM_NUMBER]),(self.config.ITEM_NUMBER,-1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden,last_layer),1)
