#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: rnn_user_attention
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018-11-26,11:43
#==================================


import numpy as np
import ipdb
from .base import basic_model
import tensorflow as tf
from .time_attention import vanilla_attention,vanilla_attention_for_all
from .rnn_user import rnn_user

class rnn_user_attention(rnn_user):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        purchased_items = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        purchased_items_time = tf.concat([purchased_items,self.purchased_time],2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     purchased_items_time,initial_state)
        context = tf.concat([tf.gather_nd(self.rnn_outputs, self.target_index),u_embedding],1)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,
                                                    self.config.LATENT_FACTOR+self.config.RNN_HIDDEN+self.config.RNN_HIDDEN), -1.0, 1.0),
                                 name='last_layer', trainable=True)

        seq_mask = tf.cast(tf.reduce_mean(tf.transpose(self.purchased_items,[1,0,2]),2),tf.bool)
        seq = tf.transpose(self.rnn_outputs,[1,0,2])

        p_atten = vanilla_attention(tf.nn.embedding_lookup(item_feature, self.positve_sample),seq,seq_mask)
        n_atten = vanilla_attention(tf.nn.embedding_lookup(item_feature, self.negative_sample),seq,seq_mask)

        pfeature = tf.nn.embedding_lookup(last_layer, self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer, self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(tf.concat([context,p_atten],1), pfeature), axis=1), (-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(tf.concat([context,n_atten],1), nfeature), axis=1), (-1,))

        expand_hidden = tf.reshape(tf.tile(context, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        all_attention_feature = tf.cond(tf.equal(tf.shape(seq)[0], 1),
                                        lambda: vanilla_attention_for_all(item_feature, seq, seq_mask),
                                        lambda: tf.zeros_like(item_feature))
        self.all_score = tf.reduce_sum(tf.multiply(tf.concat([expand_hidden,all_attention_feature],1), last_layer),1)


