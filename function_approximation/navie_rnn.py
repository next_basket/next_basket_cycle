#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: navie_rnn
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/20,下午10:19
#==================================

import numpy as np
import ipdb
from .rnn_user import rnn_user
import tensorflow as tf


class navie_rnn(rnn_user):
    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        purchased_items = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        purchased_items = tf.concat([purchased_items,self.purchased_time],2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     purchased_items,initial_state)
        context = tf.gather_nd(self.rnn_outputs, self.target_index)
        last_layer = tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER, self.config.LATENT_FACTOR), -1.0, 1.0),
                                 name='last_layer', trainable=True)
        pfeature = tf.nn.embedding_lookup(last_layer, self.positve_sample)
        nfeature = tf.nn.embedding_lookup(last_layer, self.negative_sample)
        self.pscore = tf.reshape(tf.reduce_sum(tf.multiply(context, pfeature), axis=1), (-1,))
        self.nscore = tf.reshape(tf.reduce_sum(tf.multiply(context, nfeature), axis=1), (-1,))
        expand_hidden = tf.reshape(tf.tile(context, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        self.all_score = tf.reduce_sum(tf.multiply(expand_hidden, last_layer), 1)


