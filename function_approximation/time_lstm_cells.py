#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: time_lstm_cells
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/24,下午8:03
#==================================

import numpy as np
import ipdb
import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import *
from .rnn_cells import nlstm,_clipped_value
from tensorflow.contrib.model_pruning.python.layers import core_layers
_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"

class t1lstm(nlstm):
    def build(self, inputs_shape):
        if inputs_shape[1].value is None:
            raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                             % inputs_shape)

        input_depth = inputs_shape[1].value-1
        h_depth = self._num_units
        # input gate
        self.W_xi = self.add_variable("w_xi", shape=(input_depth, h_depth))
        self.W_hi = self.add_variable("w_hi", shape=(h_depth, h_depth))
        self.bias_i = self.add_variable("bias_i", shape=(h_depth,))
        self.w_ci = self.add_variable("w_ci", shape=(h_depth,))
        # forget gate
        self.W_xf = self.add_variable("w_xf", shape=(input_depth, h_depth))
        self.W_hf = self.add_variable("w_hf", shape=(h_depth, h_depth))
        self.w_cf = self.add_variable("w_cf", shape=(h_depth,))
        self.bias_f = self.add_variable("bias_f", shape=(h_depth,))
        # cell
        self.W_xc = self.add_variable("w_xc", shape=(input_depth, h_depth))
        self.W_hc = self.add_variable("w_hc", shape=(h_depth, h_depth))
        self.bias_c = self.add_variable("bias_c", shape=(h_depth,))
        # output gate
        self.W_xo = self.add_variable("w_xo", shape=(input_depth, h_depth))
        self.W_ho = self.add_variable("w_ho", shape=(h_depth, h_depth))
        self.w_co = self.add_variable("w_co", shape=(h_depth,))
        self.bias_o = self.add_variable("bias_o", shape=(h_depth,))
        self.W_to = self.add_variable("w_to",shape=(h_depth,))

        # time gate
        self.W_xt = self.add_variable("w_xt",shape=(input_depth, h_depth))
        self.W_tt = self.add_variable("w_tt", shape=(h_depth,))
        self.bias_t = self.add_variable("bias_t",shape=(h_depth,))


        self.built = True

    def call(self, inputs, state):
        time = tf.slice(inputs, [0, tf.shape(inputs)[1] - 1], [-1, 1], name="rnn_time")
        inputs = tf.slice(inputs, [0, 0], [-1, tf.shape(inputs)[1] - 1], name="rnn_input")
        sigmoid = math_ops.sigmoid
        add = math_ops.add
        tanh = math_ops.tanh
        multiply = math_ops.multiply
        mat_mul = math_ops.mat_mul
        one = constant_op.constant(1, dtype=dtypes.int32)
        # Parameters of gates are concatenated into one multiply for efficiency.
        if self._state_is_tuple:
            c, h = state
        else:
            c, h = array_ops.split(value=state, num_or_size_splits=2, axis=one)

        # cell clipping to avoid explostion
        c = clip_ops.clip_by_value(c, -1.0*_clipped_value, 1.0*_clipped_value)

        input_gate = sigmoid(
            add(
                add(
                    add(
                        mat_mul(inputs, self.W_xi),
                        mat_mul(h, self.W_hi)),
                    multiply(c, self.w_ci)),
                self.bias_i))

        forget_gate = sigmoid(
            add(
                add(
                    add(
                        mat_mul(inputs, self.W_xf),
                        mat_mul(h, self.W_hf)),
                    multiply(c, self.w_cf)),
                self.bias_f))
        t = sigmoid(add(add(mat_mul(inputs,self.W_xt),
                            sigmoid(multiply(time,self.W_tt))),self.bias_t))
        new_c = add(multiply(forget_gate, c),multiply(multiply(input_gate,t), tanh(
            add(add(mat_mul(inputs, self.W_xc),mat_mul(h, self.W_hc)),self.bias_c))))
        output_gate = sigmoid(
            add(add(add(add(mat_mul(inputs, self.W_xo),multiply(time,self.W_to)),
                        mat_mul(h, self.W_ho)),
                    multiply(new_c, self.w_co)),
                self.bias_o))
        new_h = multiply(output_gate, tanh(new_c))

        if self._state_is_tuple:
            new_state = LSTMStateTuple(new_c, new_h)
        else:
            new_state = array_ops.concat([new_c, new_h], 1)
        return new_h, new_state

class t2lstm(nlstm):
    def build(self, inputs_shape):
        if inputs_shape[1].value is None:
            raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                             % inputs_shape)

        input_depth = inputs_shape[1].value-1
        h_depth = self._num_units
        # input gate
        self.W_xi = self.add_variable("w_xi", shape=(input_depth, h_depth))
        self.W_hi = self.add_variable("w_hi", shape=(h_depth, h_depth))
        self.bias_i = self.add_variable("bias_i", shape=(h_depth,))
        self.w_ci = self.add_variable("w_ci", shape=(h_depth,))
        # forget gate
        self.W_xf = self.add_variable("w_xf", shape=(input_depth, h_depth))
        self.W_hf = self.add_variable("w_hf", shape=(h_depth, h_depth))
        self.w_cf = self.add_variable("w_cf", shape=(h_depth,))
        self.bias_f = self.add_variable("bias_f", shape=(h_depth,))
        # cell
        self.W_xc = self.add_variable("w_xc", shape=(input_depth, h_depth))
        self.W_hc = self.add_variable("w_hc", shape=(h_depth, h_depth))
        self.bias_c = self.add_variable("bias_c", shape=(h_depth,))
        # output gate
        self.W_xo = self.add_variable("w_xo", shape=(input_depth, h_depth))
        self.W_ho = self.add_variable("w_ho", shape=(h_depth, h_depth))
        self.w_co = self.add_variable("w_co", shape=(h_depth,))
        self.bias_o = self.add_variable("bias_o", shape=(h_depth,))
        self.W_to = self.add_variable("w_to",shape=(h_depth,))

        # time1 gate
        self.W_xt1 = self.add_variable("w_xt1",shape=(input_depth, h_depth))
        self.W_tt1 = self.add_variable("w_tt1", shape=(h_depth,))
        self.bias_t1 = self.add_variable("bias_t1",shape=(h_depth,))

        # time2 gate
        self.W_xt2 = self.add_variable("w_xt2",shape=(input_depth, h_depth))
        self.W_tt2 = self.add_variable("w_tt2", shape=(h_depth,))
        self.bias_t2 = self.add_variable("bias_t2",shape=(h_depth,))

        self.built = True

    def call(self, inputs, state):
        time = tf.slice(inputs, [0, tf.shape(inputs)[1] - 1], [-1, 1], name="rnn_time")
        inputs = tf.slice(inputs, [0, 0], [-1, tf.shape(inputs)[1] - 1], name="rnn_input")
        sigmoid = math_ops.sigmoid
        add = math_ops.add
        tanh = math_ops.tanh
        multiply = math_ops.multiply
        mat_mul = math_ops.mat_mul
        one = constant_op.constant(1, dtype=dtypes.int32)
        # Parameters of gates are concatenated into one multiply for efficiency.
        if self._state_is_tuple:
            c, h = state
        else:
            c, h = array_ops.split(value=state, num_or_size_splits=2, axis=one)

        # cell clipping to avoid explostion
        c = clip_ops.clip_by_value(c, -1.0*_clipped_value, 1.0*_clipped_value)

        input_gate = sigmoid(
            add(
                add(
                    add(
                        mat_mul(inputs, self.W_xi),
                        mat_mul(h, self.W_hi)),
                    multiply(c, self.w_ci)),
                self.bias_i))

        forget_gate = sigmoid(
            add(
                add(
                    add(
                        mat_mul(inputs, self.W_xf),
                        mat_mul(h, self.W_hf)),
                    multiply(c, self.w_cf)),
                self.bias_f))
        t1 = sigmoid(add(add(mat_mul(inputs,math_ops.abs(self.W_xt1)),
                            sigmoid(multiply(time,self.W_tt1))),self.bias_t1))
        t2 = sigmoid(add(add(mat_mul(inputs,math_ops.abs(self.W_xt2)),
                            sigmoid(multiply(time,self.W_tt2))),self.bias_t2))
        bar_c = add(multiply(forget_gate, c),multiply(multiply(input_gate,t1), tanh(
            add(add(mat_mul(inputs, self.W_xc),mat_mul(h, self.W_hc)),self.bias_c))))
        new_c = add(multiply(forget_gate, c), multiply(multiply(input_gate, t2), tanh(
            add(add(mat_mul(inputs, self.W_xc), mat_mul(h, self.W_hc)), self.bias_c))))
        output_gate = sigmoid(
            add(add(add(add(mat_mul(inputs, self.W_xo),multiply(time,self.W_to)),
                        mat_mul(h, self.W_ho)),
                    multiply(bar_c, self.w_co)),
                self.bias_o))
        new_h = multiply(output_gate, tanh(bar_c))
        if self._state_is_tuple:
            new_state = LSTMStateTuple(new_c, new_h)
        else:
            new_state = array_ops.concat([new_c, new_h], 1)
        return new_h, new_state

class t3lstm(t2lstm):
    def build(self, inputs_shape):
        if inputs_shape[1].value is None:
            raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                             % inputs_shape)

        input_depth = inputs_shape[1].value-1
        h_depth = self._num_units
        # input gate
        self.W_xi = self.add_variable("w_xi", shape=(input_depth, h_depth))
        self.W_hi = self.add_variable("w_hi", shape=(h_depth, h_depth))
        self.bias_i = self.add_variable("bias_i", shape=(h_depth,))
        self.w_ci = self.add_variable("w_ci", shape=(h_depth,))

        # cell
        self.W_xc = self.add_variable("w_xc", shape=(input_depth, h_depth))
        self.W_hc = self.add_variable("w_hc", shape=(h_depth, h_depth))
        self.bias_c = self.add_variable("bias_c", shape=(h_depth,))
        # output gate
        self.W_xo = self.add_variable("w_xo", shape=(input_depth, h_depth))
        self.W_ho = self.add_variable("w_ho", shape=(h_depth, h_depth))
        self.w_co = self.add_variable("w_co", shape=(h_depth,))
        self.bias_o = self.add_variable("bias_o", shape=(h_depth,))
        self.W_to = self.add_variable("w_to",shape=(h_depth,))

        # time1 gate
        self.W_xt1 = self.add_variable("w_xt1",shape=(input_depth, h_depth))
        self.W_tt1 = self.add_variable("w_tt1", shape=(h_depth,))
        self.bias_t1 = self.add_variable("bias_t1",shape=(h_depth,))

        # time2 gate
        self.W_xt2 = self.add_variable("w_xt2",shape=(input_depth, h_depth))
        self.W_tt2 = self.add_variable("w_tt2", shape=(h_depth,))
        self.bias_t2 = self.add_variable("bias_t2",shape=(h_depth,))

        self.built = True
    def call(self, inputs, state):
        time = tf.slice(inputs, [0, tf.shape(inputs)[1] - 1], [-1, 1], name="rnn_time")
        inputs = tf.slice(inputs, [0, 0], [-1, tf.shape(inputs)[1] - 1], name="rnn_input")
        sigmoid = math_ops.sigmoid
        add = math_ops.add
        tanh = math_ops.tanh
        multiply = math_ops.multiply
        mat_mul = math_ops.mat_mul
        one = constant_op.constant(1, dtype=dtypes.int32)
        # Parameters of gates are concatenated into one multiply for efficiency.
        if self._state_is_tuple:
            c, h = state
        else:
            c, h = array_ops.split(value=state, num_or_size_splits=2, axis=one)

        # cell clipping to avoid explostion
        c = clip_ops.clip_by_value(c, -1.0, 1.0)

        input_gate = sigmoid(
            add(
                add(
                    add(
                        mat_mul(inputs, self.W_xi),
                        mat_mul(h, self.W_hi)),
                    multiply(c, self.w_ci)),
                self.bias_i))

        t1 = sigmoid(add(add(mat_mul(inputs,math_ops.abs(self.W_xt1)),
                            sigmoid(multiply(time,self.W_tt1))),self.bias_t1))
        t2 = sigmoid(add(add(mat_mul(inputs,math_ops.abs(self.W_xt2)),
                            sigmoid(multiply(time,self.W_tt2))),self.bias_t2))
        bar_c = add(multiply(c,(1-multiply(input_gate,t1))),multiply(multiply(input_gate,t1), tanh(
            add(add(mat_mul(inputs, self.W_xc),mat_mul(h, self.W_hc)),self.bias_c))))
        new_c = add(multiply((1-input_gate),c), multiply(multiply(input_gate, t2), tanh(
            add(add(mat_mul(inputs, self.W_xc), mat_mul(h, self.W_hc)), self.bias_c))))
        output_gate = sigmoid(
            add(add(add(add(mat_mul(inputs, self.W_xo),multiply(time,self.W_to)),
                        mat_mul(h, self.W_ho)),
                    multiply(bar_c, self.w_co)),
                self.bias_o))
        new_h = multiply(output_gate, tanh(bar_c))
        if self._state_is_tuple:
            new_state = LSTMStateTuple(new_c, new_h)
        else:
            new_state = array_ops.concat([new_c, new_h], 1)
        return new_h, new_state
