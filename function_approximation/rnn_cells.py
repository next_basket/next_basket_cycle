#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: rnn_cells
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/24,上午10:04
#==================================
import numpy as np
import ipdb
import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import *
from tensorflow.contrib.model_pruning.python.layers import core_layers
_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"
_clipped_value = 100000000.0
class nlstm(tf.nn.rnn_cell.BasicLSTMCell):
    @property
    def state_size(self):
        return (LSTMStateTuple(self._num_units, self._num_units)
                if self._state_is_tuple else 2 * self._num_units)

    @property
    def output_size(self):
        return self._num_units


    def build(self, inputs_shape):
        print("ntlstm cell")
        if inputs_shape[1].value is None:
            raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                             % inputs_shape)

        input_depth = inputs_shape[1].value-1
        h_depth = self._num_units
        # input gate
        self.W_xi = self.add_variable("w_xi",shape=(input_depth, h_depth))
        self.W_hi = self.add_variable("w_hi",shape=(h_depth, h_depth))
        self.bias_i = self.add_variable("bias_i",shape=(h_depth,))
        self.w_ci = self.add_variable("w_ci",shape=(h_depth,))
        # forget gate
        self.W_xf = self.add_variable("w_xf",shape=(input_depth, h_depth))
        self.W_hf = self.add_variable("w_hf",shape=(h_depth, h_depth))
        self.w_cf = self.add_variable("w_cf",shape=(h_depth,))
        self.bias_f = self.add_variable("bias_f",shape=(h_depth,))
        # cell
        self.W_xc = self.add_variable("w_xc",shape=(input_depth, h_depth))
        self.W_hc = self.add_variable("w_hc",shape=(h_depth, h_depth))
        self.bias_c = self.add_variable("bias_c",shape=(h_depth,))
        # output gate
        self.W_xo = self.add_variable("w_xo",shape=(input_depth, h_depth))
        self.W_ho = self.add_variable("w_ho",shape=(h_depth, h_depth))
        self.w_co = self.add_variable("w_co",shape=(h_depth,))
        self.bias_o= self.add_variable("bias_o",shape=(h_depth,))

        self.built = True

    def call(self, inputs, state):
        time = tf.slice(inputs, [0, tf.shape(inputs)[1] - 1], [-1, 1], name="rnn_time")
        inputs = tf.slice(inputs, [0, 0], [-1, tf.shape(inputs)[1] - 1], name="rnn_input")
        sigmoid = math_ops.sigmoid
        one = constant_op.constant(1, dtype=dtypes.int32)
        # Parameters of gates are concatenated into one multiply for efficiency.
        if self._state_is_tuple:
            c, h = state
        else:
            c, h = array_ops.split(value=state, num_or_size_splits=2, axis=one)

        # cell clipping to avoid explostion
        c = clip_ops.clip_by_value(c, -1.0*_clipped_value, 1.0*_clipped_value)

        input_gate = sigmoid(
            math_ops.add(
                math_ops.add(
                    math_ops.add(
                        math_ops.mat_mul(inputs,self.W_xi),
                        math_ops.mat_mul(h,self.W_hi)),
                    math_ops.multiply(c,self.w_ci)),
                self.bias_i))

        forget_gate = sigmoid(
            math_ops.add(
                math_ops.add(
                    math_ops.add(
                        math_ops.mat_mul(inputs,self.W_xf),
                        math_ops.mat_mul(h,self.W_hf)),
                    math_ops.multiply(c,self.w_cf)),
                self.bias_f))
        new_c = math_ops.add(
            math_ops.multiply(forget_gate,c),
            math_ops.multiply(
                input_gate,math_ops.tanh(
                    math_ops.add(math_ops.add(
                        math_ops.mat_mul(inputs,self.W_xc),
                        math_ops.mat_mul(h,self.W_hc)),
                        self.bias_c))))
        output_gate = sigmoid(
            math_ops.add(
                math_ops.add(
                    math_ops.add(
                        math_ops.mat_mul(inputs,self.W_xo),
                        math_ops.mat_mul(h,self.W_ho)),
                    math_ops.multiply(new_c,self.w_co)),
                self.bias_o))
        new_h = math_ops.multiply(output_gate,math_ops.tanh(new_c))

        if self._state_is_tuple:
            new_state = LSTMStateTuple(new_c, new_h)
        else:
            new_state = array_ops.concat([new_c, new_h], 1)
        return new_h, new_state

class ctlstm(nlstm):
    def build(self, inputs_shape):
        print("ctlstm cell")
        if inputs_shape[1].value is None:
            raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                             % inputs_shape)

        input_depth = inputs_shape[1].value-1
        h_depth = self._num_units
        # input gate
        self.W_xi = self.add_variable("w_xi",shape=(input_depth, h_depth))
        self.W_hi = self.add_variable("w_hi",shape=(h_depth, h_depth))
        self.bias_i = self.add_variable("bias_i",shape=(h_depth,))

        # forget gate
        self.W_xf = self.add_variable("w_xf",shape=(input_depth, h_depth))
        self.W_hf = self.add_variable("w_hf",shape=(h_depth, h_depth))
        self.bias_f = self.add_variable("bias_f",shape=(h_depth,))

        # input bar gate
        self.W_xbari = self.add_variable("W_xbari",shape=(input_depth, h_depth))
        self.W_hbari = self.add_variable("W_hbari",shape=(h_depth, h_depth))
        self.bias_bari = self.add_variable("bias_bari",shape=(h_depth,))

        # forget bar gate
        self.W_xbarf = self.add_variable("W_xbarf",shape=(input_depth, h_depth))
        self.W_hbarf = self.add_variable("W_hbarf",shape=(h_depth, h_depth))
        self.bias_barf = self.add_variable("bias_barf",shape=(h_depth,))

        # reset gate
        self.W_xz = self.add_variable("w_xz",shape=(input_depth, h_depth))
        self.W_hz = self.add_variable("w_hz",shape=(h_depth, h_depth))
        self.bias_z = self.add_variable("bias_z",shape=(h_depth,))

        # output gate
        self.W_xo = self.add_variable("w_xo",shape=(input_depth, h_depth))
        self.W_ho = self.add_variable("w_ho",shape=(h_depth, h_depth))
        self.bias_o= self.add_variable("bias_o",shape=(h_depth,))

        # delta
        self.W_xdelta = self.add_variable("w_xdelta",shape=(input_depth, h_depth))
        self.W_hdelta = self.add_variable("w_hdelta", shape=(h_depth, h_depth))
        self.bias_delta= self.add_variable("bias_delta",shape=(h_depth,))

        self.built = True

    def call(self, inputs, state):
        time = tf.slice(inputs, [0, tf.shape(inputs)[1] - 1], [-1, 1], name="rnn_time")
        inputs = tf.slice(inputs, [0, 0], [-1, tf.shape(inputs)[1] - 1], name="rnn_input")
        sigmoid = math_ops.sigmoid
        add = math_ops.add
        tanh = math_ops.tanh
        mat_mul = math_ops.mat_mul
        multiply = math_ops.multiply
        one = constant_op.constant(1, dtype=dtypes.int32)
        # Parameters of gates are concatenated into one multiply for efficiency.
        if self._state_is_tuple:
            c,h = state
        else:
            c,h = array_ops.split(value=state, num_or_size_splits=2, axis=one)
        c = clip_ops.clip_by_value(c, -1.0*_clipped_value, 1.0*_clipped_value)
        c,c_bar = array_ops.split(value=c,num_or_size_splits=2,axis=one)

        i = sigmoid(add(add(mat_mul(inputs,self.W_xi),mat_mul(h,self.W_hi)),self.bias_i))
        f = sigmoid(add(add(mat_mul(inputs,self.W_xf),mat_mul(h,self.W_hf)),self.bias_f))
        z = multiply(2.0,sigmoid(add(add(mat_mul(inputs,self.W_xz),mat_mul(h,self.W_hz)),self.bias_z)))-1
        o = sigmoid(add(add(mat_mul(inputs,self.W_xo),mat_mul(h,self.W_ho)),self.bias_o))
        bar_i = sigmoid(add(add(mat_mul(inputs,self.W_xbari),mat_mul(h,self.W_hbari)),self.bias_bari))
        bar_f = sigmoid(add(add(mat_mul(inputs,self.W_xbarf),mat_mul(h,self.W_hbarf)),self.bias_barf))
        c = add(multiply(f,c),multiply(i,z))
        bar_c = add(multiply(bar_f,c_bar),multiply(bar_i,z))
        # not sure about the f
        delta = tanh(add(add(mat_mul(inputs,self.W_xdelta),mat_mul(h,self.W_hdelta)),self.bias_delta))
        new_c = add(bar_c,multiply((c-bar_c),math_ops.exp(-multiply(delta,time))))
        new_h = multiply(o,2.0*sigmoid(2.0*new_c)-1)
        if self._state_is_tuple:
            new_state = LSTMStateTuple(array_ops.concat([new_c,bar_c],1), new_h)
        else:
            new_state = array_ops.concat([new_c, new_h], 1)
        return new_h, new_state


