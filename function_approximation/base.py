#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: base
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/20,上午2:24
#==================================

#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: base
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午4:06
#==================================
import numpy as np
import ipdb
import tensorflow as tf
from utils.zlog import log
from .rnn_cells import nlstm,ctlstm
from .time_lstm_cells import t1lstm,t2lstm,t3lstm

class basic_model(object):
    GRAPHS = {}
    SESS = {}
    SAVER = {}

    @classmethod
    def create_model(cls, config, variable_scope = "target", trainable = True, graph_name="DEFAULT"):
        log.info("CREATE MODEL", config.model, "GRAPH", graph_name, "VARIABLE SCOPE", variable_scope)
        if not graph_name in cls.GRAPHS:
            log.info("Adding a new tensorflow graph:",graph_name)
            cls.GRAPHS[graph_name] = tf.Graph()
        with cls.GRAPHS[graph_name].as_default():
            model = cls(config, variable_scope=variable_scope, trainable=trainable)
            if not graph_name in cls.SESS:
                cls.SESS[graph_name] = tf.Session(config = tf.ConfigProto(gpu_options=config.GPU_OPTION))
                cls.SAVER[graph_name] = tf.train.Saver(max_to_keep=50)
            cls.SESS[graph_name].run(model.init)
        return {"graph": cls.GRAPHS[graph_name],
               "sess": cls.SESS[graph_name],
               "saver": cls.SAVER[graph_name],
               "model": model}

    def _update_placehoders(self):
        self.placeholders = {"none":{}}
        raise NotImplemented

    def _get_feed_dict(self,task,data_dicts):
        place_holders = self.placeholders[task]
        res = {}
        for key, value in place_holders.items():
            res[value] = data_dicts[key]
        return res

    def __init__(self, config, variable_scope = "target", trainable = True):
        print(self.__class__)
        self.config = config
        self.variable_scope = variable_scope
        self.trainable = trainable
        self.placeholders = {}
        self._build_model()

    def _build_model(self):
        with tf.variable_scope(self.variable_scope):
            self._create_placeholders()
            self._create_global_step()
            self._update_placehoders()
            self._create_inference()
            if self.trainable:
                self._create_optimizer()
            self._create_intializer()

    def _create_global_step(self):
        self.global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')

    def _create_intializer(self):
        with tf.name_scope("initlializer"):
            self.init = tf.global_variables_initializer()

    def _create_placeholders(self):
        raise NotImplementedError

    def _create_inference(self):
        raise NotImplementedError

    def _create_optimizer(self):
        raise NotImplementedError

    def build_cell(self,rnn_type,initializer,hidden,input_data,initial_state):
        if rnn_type == "lstm":
            cell = tf.contrib.rnn.MultiRNNCell(
                [tf.nn.rnn_cell.LSTMCell(hidden,
                                         cell_clip=3,
                                         initializer=initializer)]*self.config.RNN_LAYER,state_is_tuple=True)
            return tf.nn.dynamic_rnn(cell,inputs = input_data,
                                     initial_state = (tf.nn.rnn_cell.LSTMStateTuple(c = tf.zeros_like(initial_state[0]),h = initial_state[0]),),
                                     dtype=tf.float32,time_major=True)


        elif rnn_type == "gru":
            cell = tf.contrib.rnn.MultiRNNCell([tf.nn.rnn_cell.GRUCell(hidden,
                                          kernel_initializer=initializer,
                                          bias_initializer=tf.zeros_initializer)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell,input_data,initial_state=initial_state,
                                                             dtype=tf.float32,
                                                             time_major=True)
        elif rnn_type == "nlstm":
            cell = tf.contrib.rnn.MultiRNNCell([nlstm(hidden)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell, input_data,
                                     initial_state=(tf.nn.rnn_cell.LSTMStateTuple(c=tf.zeros_like(initial_state[0]),
                                                                                  h=initial_state[0]),),
                                     dtype=tf.float32,
                                     time_major=True)
        elif rnn_type == "t1lstm":
            cell = tf.contrib.rnn.MultiRNNCell([t1lstm(hidden)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell, input_data,
                                     initial_state=(tf.nn.rnn_cell.LSTMStateTuple(c=tf.zeros_like(initial_state[0]),
                                                                                  h=initial_state[0]),),
                                     dtype=tf.float32,
                                     time_major=True)
        elif rnn_type == "t2lstm":
            cell = tf.contrib.rnn.MultiRNNCell([t2lstm(hidden)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell, input_data,
                                     initial_state=(tf.nn.rnn_cell.LSTMStateTuple(c=tf.zeros_like(initial_state[0]),
                                                                                  h=initial_state[0]),),
                                     dtype=tf.float32,
                                     time_major=True)
        elif rnn_type == "t3lstm":
            cell = tf.contrib.rnn.MultiRNNCell([t3lstm(hidden)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell, input_data,
                                     initial_state=(tf.nn.rnn_cell.LSTMStateTuple(c=tf.zeros_like(initial_state[0]),
                                                                                  h=initial_state[0]),),
                                     dtype=tf.float32,
                                     time_major=True)
        elif rnn_type == "ctlstm":
            cell = tf.contrib.rnn.MultiRNNCell([ctlstm(hidden)]*self.config.RNN_LAYER)
            return tf.nn.dynamic_rnn(cell, input_data,
                                     initial_state=(tf.nn.rnn_cell.LSTMStateTuple(c=tf.concat([tf.zeros_like(initial_state[0],dtype=tf.float32),
                                                                                               tf.zeros_like(initial_state[0],dtype=tf.float32)],1),
                                                                                  h=initial_state[0]),),
                                     dtype=tf.float32,
                                     time_major=True)

