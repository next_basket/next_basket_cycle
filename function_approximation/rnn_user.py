#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: rnn_user
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/24,上午10:32
#==================================

import numpy as np
import ipdb
from .base import basic_model
import tensorflow as tf
from .time_attention import vanilla_attention

def non_linear(x):
    return x

def non_linear_two(x):
    return x

class rnn_user(basic_model):
    def _create_placeholders(self):
        self.uid = tf.placeholder(tf.int32,(None,))
        self.purchased_items = tf.placeholder(tf.int32,(None,None,1))
        self.purchased_time = tf.placeholder(tf.float32,(None,None,1))
        self.target_index = tf.placeholder(tf.int32,(None,2))
        self.positve_sample = tf.placeholder(tf.int32,(None,))
        self.negative_sample = tf.placeholder(tf.int32,(None,))
        self.labels = tf.placeholder(tf.float32,(None,))


    def _update_placehoders(self):
        self.placeholders["all"] = {"uid":self.uid,
                                    "purchased_items":self.purchased_items,
                                    "target_index":self.target_index,
                                    "positive_sample":self.positve_sample,
                                    "negative_sample":self.negative_sample,
                                    "labels":self.labels,
                                    "purchased_time":self.purchased_time}
        predicts = ["uid","purchased_items","target_index","positive_sample","purchased_time"]
        predicts_all = ["uid","purchased_items","target_index","purchased_time"]
        self.placeholders["predict"]={item:self.placeholders["all"][item] for item in predicts}
        self.placeholders["optimizer"] = self.placeholders["all"]
        self.placeholders["predict_all"] = {item:self.placeholders["all"][item] for item in predicts_all}

    def _create_inference(self):
        initializer = tf.random_uniform_initializer(0, 0.1, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        # u_embedding = tf.multiply(u_embedding,tf.zeros_like(u_embedding))
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        purchased_items = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)
        purchased_items_time = tf.concat([purchased_items,self.purchased_time],2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     purchased_items_time,initial_state)
        # self.rnn_outputs = tf.multiply(self.rnn_outputs,tf.zeros_like(self.rnn_outputs))
        rnn_out = tf.gather_nd(self.rnn_outputs, self.target_index)
        last_layer = [tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,
                                                    self.config.LATENT_FACTOR), 0.0, 0.1),
                                 name='last_layer', trainable=True) for i in range(2)]
        pfeature = [tf.nn.embedding_lookup(last_layer[i], self.positve_sample) for i in range(2)]
        nfeature = [tf.nn.embedding_lookup(last_layer[i], self.negative_sample) for i in range(2)]
        w1,w2 = 0.5,0.5
        print("weight",w1,w2)
        self.pscore = tf.reshape(tf.reduce_sum(
            w1*non_linear(tf.multiply(u_embedding, pfeature[0]))+w2*non_linear_two(tf.multiply(rnn_out, pfeature[1]))
            , axis=1), (-1,))
        self.nscore = tf.reshape(tf.reduce_sum(
            w1*non_linear(tf.multiply(u_embedding, nfeature[0]))+w2*non_linear_two(tf.multiply(rnn_out, nfeature[1])), axis=1), (-1,))
        expand_rnn = tf.reshape(tf.tile(rnn_out, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        expand_user = tf.reshape(tf.tile(u_embedding, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        self.all_score = tf.reduce_sum(
            w1*non_linear(tf.multiply(expand_user, last_layer[0]))+w2*non_linear_two(tf.multiply(expand_rnn, last_layer[1])), 1)
        self.regularize_model = tf.nn.l2_loss(item_feature)+\
                           tf.nn.l2_loss(user_feature)+\
                           tf.nn.l2_loss(last_layer[0])+tf.nn.l2_loss(last_layer[1])

    def _last_layer(self,context):
        return tf.layers.dense(context, self.config.ITEM_NUMBER,
                        bias_initializer=tf.zeros_initializer,
                        trainable=self.trainable, activation=tf.nn.softmax)

    def _create_optimizer(self):
        sample_score = self.pscore-self.nscore
        self.loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_score,labels=self.labels))
        print("#"*10,self.config.regularizer)
        self.loss = self.loss+ self.config.regularizer*self.regularize_model
        optimizer = self.config.optimizer_name(self.config.learning_rate)
        gvs = optimizer.compute_gradients(self.loss)
        capped_gvs = [(tf.clip_by_value(grad, -1.0, 1.0), var) for grad, var in gvs]
        self.optimizer = optimizer.apply_gradients(capped_gvs,global_step=self.global_step)

    def optimize_model(self,sess,data):
        feed_dicts = self._get_feed_dict("optimizer",data)
        return sess.run([self.loss,self.optimizer],feed_dicts)[0]

    def predict(self,sess,data):
        feed_dicts = self._get_feed_dict("predict", data)
        return sess.run(self.all_score, feed_dicts)

    def predict_all_score(self,sess,data):
        # for predict all the data must be one dimension
        feed_dicts = self._get_feed_dict("predict_all", data)
        return sess.run(self.all_score, feed_dicts)

    def debug_model(self,sess,data):
        feed_dicts = self._get_feed_dict("optimizer",data)
        ipdb.set_trace()
        return sess.run([self.loss,self.optimizer],feed_dicts)[0]


class bpr_mf(rnn_user):
    def _create_inference(self):
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='item_feature')
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     0, 0.1),trainable=self.trainable,name='user_feature')
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        pfeature = tf.nn.embedding_lookup(item_feature, self.positve_sample)
        nfeature = tf.nn.embedding_lookup(item_feature, self.negative_sample)
        self.pscore = non_linear(tf.reshape(tf.reduce_sum(tf.multiply(u_embedding, pfeature), axis=1), (-1,)))
        self.nscore = non_linear(tf.reshape(tf.reduce_sum(tf.multiply(u_embedding, nfeature), axis=1), (-1,)))
        expand_hidden = tf.reshape(tf.tile(u_embedding, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        self.all_score = non_linear(tf.reduce_sum(tf.multiply(expand_hidden, item_feature), 1))
        self.regularizer = self.regularizer = tf.nn.l2_loss(item_feature)+\
                           tf.nn.l2_loss(user_feature)

    def _create_optimizer(self):
        sample_score = self.pscore-self.nscore
        self.loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_score,labels=self.labels))+\
                    self.config.regularizer*self.regularizer
        optimizer = self.config.optimizer_name(self.config.learning_rate)
        gvs = optimizer.compute_gradients(self.loss)
        self.gvs = gvs
        # capped_gvs = [(tf.clip_by_value(grad, -1.0, 1.0), var) for grad, var in gvs]
        self.optimizer = optimizer.apply_gradients(gvs,global_step=self.global_step)