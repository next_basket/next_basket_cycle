#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: time_attention
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018-11-25,21:16
#==================================

import numpy as np
import ipdb
import tensorflow as tf

import tensorflow as tf



weight4time = 1.0
weight4sim = 1.0
max_d_ij = 10.0
def for_time_attention(t):
    return tf.log(tf.nn.relu(t)+0.1)
    # return tf.log(tf.nn.relu(t)+0.1)
    # return tf.sigmoid(t/max_d_ij)

def vanilla_attention(item,seq,seq_mask,**args):
    """
    :param item:[batch,embedding]
    :param seq:[batch,seq_length,embedding]
    :param seq_mask:[batch,seq_length]
    :return:[batch,embedding]
    """
    seq_mask = tf.expand_dims(seq_mask, axis=1) # s*1*seq_length
    item = tf.expand_dims(item, axis=1)  # B*1*H
    sim_matrix = tf.matmul(item, tf.transpose(seq, [0, 2, 1]))  # B*1*L
    padding = tf.ones_like(sim_matrix) * (-2 ** 32 + 1)
    atten = tf.where(seq_mask, sim_matrix, padding)
    score = tf.nn.softmax(atten) # B*1*L
    attention = tf.reshape(tf.matmul(score,seq), (tf.shape(atten)[0],-1))
    return attention

def time_attention(item,seq,seq_mask,item_id,seq_id,categories,cycle_matrix,purchased_time):
    """
    :param item: [batch, embedding]
    :param seq: [batch,seq_length,embedding]
    :param seq_mask: seq_mask:[batch,seq_length]
    :param item_id: [batch,]
    :param seq_id: [batch,length,1]
    :param categories: [item_number,],items category
    :param cycle_matrix: [category,category], d_{ci,cj}
    :param purchased_time: [batch,seq_length,time]
    :return: context [batch,embedding]
    """
    # seq_mask = tf.expand_dims(seq_mask, axis=1) # s*1*seq_length
    # item = tf.expand_dims(item, axis=1)  # B*1*H
    # sim_matrix = tf.matmul(item, tf.transpose(seq, [0, 2, 1]))  # B*1*L
    # # attention = tf.reshape(tf.matmul(score,seq), (tf.shape(atten)[0],-1))
    # time = tf.reverse(tf.cumsum(tf.reduce_mean(purchased_time, -1),1),[-1])
    # item_id = tf.tile(tf.reshape(tf.nn.embedding_lookup(categories,item_id), (-1, 1)), [1, tf.shape(seq_mask)[2]]) # batch,length
    # seq_id  = tf.reduce_mean(tf.nn.embedding_lookup(categories,seq_id),-1)
    # ij = tf.transpose(tf.concat([tf.reshape(seq_id, (1, -1)), tf.reshape(item_id, (1, -1))],0,name="cancat_test"), [1, 0])
    # d_ij = tf.reshape(tf.gather_nd(cycle_matrix, ij), (tf.shape(seq_mask)[0], -1))
    # score = weight4sim*sim_matrix - weight4time*tf.expand_dims(for_time_attention(d_ij-time),axis=1)
    # padding = tf.ones_like(sim_matrix) * (-2 ** 32 + 1)  # B*1*L
    # # padding = tf.zeros_like(sim_matrix)
    # score = tf.where(seq_mask,score,padding)
    # atten = tf.nn.softmax(score,-1)
    # attention = tf.reshape(tf.matmul(atten, seq), (tf.shape(atten)[0], -1))
    ###########
    seq_mask = tf.expand_dims(seq_mask, axis=1)  # s*1*seq_length
    item = tf.expand_dims(item, axis=1)  # B*1*H
    sim_matrix = tf.matmul(item, tf.transpose(seq, [0, 2, 1]))  # B*1*L
    # attention = tf.reshape(tf.matmul(score,seq), (tf.shape(atten)[0],-1))
    time = tf.cumsum(tf.reduce_mean(purchased_time, -1),1,reverse=True)
    item_id = tf.tile(tf.reshape(tf.nn.embedding_lookup(categories, item_id), (-1, 1)),
                      [1, tf.shape(seq_mask)[2]])  # batch,length
    seq_id = tf.reduce_mean(tf.nn.embedding_lookup(categories, seq_id), -1)
    ij = tf.transpose(tf.concat([tf.reshape(seq_id, (1, -1)), tf.reshape(item_id, (1, -1))], 0, name="cancat_test"),
                      [1, 0])
    d_ij = tf.reshape(tf.gather_nd(cycle_matrix, ij), (tf.shape(seq_mask)[0], -1))
    score = sim_matrix - tf.expand_dims(for_time_attention(d_ij - time), axis=1)
    padding = tf.ones_like(sim_matrix) * (-2 ** 32 + 1)  # B*1*L
    score = tf.where(seq_mask, score, padding)
    atten = tf.nn.softmax(score, -1)
    attention = tf.reshape(tf.matmul(atten, seq), (tf.shape(atten)[0], -1))
    return attention,sim_matrix,time,atten,d_ij,score,ij,for_time_attention(d_ij-time)

def vanilla_attention_for_all(item, seq, seq_mask, **args):
    """
    :param item: [item_number,embedding]
    :param seq: [1,seq_length,embedding]
    :param seq_mask: [1,seq_length]
    :return: [item_number,seq_length]
    """
    sim_matrix = tf.reduce_mean(tf.tensordot(item, seq, axes=[[1], [2]]), 1)  # [item_number,seq_length]
    seq_mask = tf.tile(seq_mask, [tf.shape(item)[0], 1])  # item_number,length
    padding = tf.ones_like(sim_matrix) * (-2 ** 32 + 1)  # item_number,length
    atten = tf.nn.softmax(tf.where(seq_mask, sim_matrix, padding), 1)  # item_item,atten_vec
    context = tf.tensordot(atten, tf.reshape(seq, (tf.shape(seq)[1], -1)), [[1], [0]])  # item_number,attention
    return context

def time_attention_all(item, seq, seq_mask, item_id, seq_id, categories, cycle_matrix, purchased_time):
    """
    :param item:
    :param seq:
    :param seq_mask:
    :param item_id:
    :param seq_id:
    :param categories:
    :param cycle_matrix:
    :param purchased_time:
    :return:
    """
    sim_matrix = tf.reduce_mean(tf.tensordot(item, seq, axes=[[1], [2]]), 1)  # [item_number,seq_length]
    seq_mask = tf.tile(seq_mask, [tf.shape(item)[0], 1])  # item_number,length
    # attention = tf.reshape(tf.matmul(score,seq), (tf.shape(atten)[0],-1))
    time = tf.cumsum(tf.reduce_mean(purchased_time, -1),1,reverse=True)
    item_id = tf.tile(tf.reshape(tf.nn.embedding_lookup(categories, item_id), (-1, 1)),
                      [1, tf.shape(seq_mask)[1]])  # batch,length
    seq_id = tf.reduce_mean(tf.tile(tf.nn.embedding_lookup(categories, seq_id),[tf.shape(item_id)[0],1,1]),-1)
    ij = tf.transpose(tf.concat([tf.reshape(seq_id, (1, -1)), tf.reshape(item_id, (1, -1))], 0, name="cancat_test"),
                      [1, 0])
    d_ij = tf.reshape(tf.gather_nd(cycle_matrix, ij), (tf.shape(seq_mask)[0], -1))
    score = weight4sim*sim_matrix - weight4time*(for_time_attention(d_ij-time))
    padding = tf.ones_like(sim_matrix) * (-2 ** 32 + 1)  # B*1*L
    score = tf.where(seq_mask, score, padding)
    atten = tf.nn.softmax(score, -1)
    context = tf.tensordot(atten, tf.reshape(seq, (tf.shape(seq)[1], -1)), [[1], [0]])
    return context

# x = tf.constant(list(range(5)))
# d = tf.constant(np.random.uniform(0,1,(5,5)))
# iid = tf.constant([1,2],dtype=tf.int32)
#
# seq_item = tf.constant(np.random.choice(range(5),(1,3,1)),dtype=tf.int32)
# # iid_index = tf.tile(tf.reshape(iid,(tf.shape(iid)[0],1)),[1,tf.shape(seq_item)[1]])
# # iiii = tf.transpose(tf.concat([tf.reshape(seq_item,(1,-1)),tf.reshape(iid_index,(1,-1))],0),[1,0])
# # d_ij = tf.reshape(tf.gather_nd(d,iiii),(tf.shape(seq_item)[0],-1))
# # time = tf.reduce_mean(tf.constant(np.random.choice(range(5),(2,3,1)),dtype=tf.float64),-1)
# # cycle  = d_ij - time
# # n_cycle = tf.nn.relu(cycle)
# #
# sess = tf.Session()
# # print(sess.run(tf.nn.embedding_lookup(x,iid)))
# # print(sess.run(seq_item))
# # print(sess.run(tf.nn.embedding_lookup(x,seq_item)))
# # print(sess.run(time))
# # print(sess.run(tf.cumsum(time,1)))
# # print(sess.run(d))
# print(sess.run(seq_item))
# print(sess.run(tf.tile(seq_item,[10,1,1])).shape)
# print(sess.run(iid_index))
# print(sess.run(cycle))
# print(sess.run(n_cycle))
# print(sess.run(iiii))
# print(sess.run(d))
# print(sess.run(d_ij))