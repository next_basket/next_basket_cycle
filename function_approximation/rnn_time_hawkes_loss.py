#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: rnn_time_attention
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/26,下午4:01
#==================================

import numpy as np
import ipdb

import numpy as np
import ipdb
from .base import basic_model
import tensorflow as tf
from .time_attention import vanilla_attention,vanilla_attention_for_all,time_attention,time_attention_all,max_d_ij
from .rnn_user import rnn_user

def non_linear(x):
    return x

class hawkes_cnn_rnn_attentive_model(rnn_user):
    def _create_placeholders(self):
        self.uid = tf.placeholder(tf.int32,(None,))
        self.purchased_items = tf.placeholder(tf.int32,(None,None,1))
        self.purchased_time = tf.placeholder(tf.float32,(None,None,1))
        self.target_index = tf.placeholder(tf.int32,(None,2))
        self.target_sample = tf.placeholder(tf.int32,(None,))
        self.delta_t = tf.placeholder(tf.int32,(None,))


    def _update_placehoders(self):
        self.placeholders["all"] = {"uid":self.uid,
                                    "purchased_items":self.purchased_items,
                                    "target_index":self.target_index,
                                    "target_sample":self.target_sample,
                                    "purchased_time":self.purchased_time}
        predicts = ["uid","purchased_items","target_index","target_sample","purchased_time"]
        predicts_all = ["uid","purchased_items","target_index","purchased_time"]
        self.placeholders["predict"]={item:self.placeholders["all"][item] for item in predicts}
        self.placeholders["optimizer"] = self.placeholders["all"]
        self.placeholders["predict_all"] = {item:self.placeholders["all"][item] for item in predicts_all}


    def _load_category(self):
        category = np.zeros((self.config.ITEM_NUMBER,),dtype=np.int32)
        with open(self.config.CATE_PATH, "r") as f:
            for line in f.readlines():
                line = line.strip("\n").split("\t")
                category[int(line[0]) - 1] = int(line[1]) - 1
        return category


    def _create_inference(self):
        initializer = tf.random_uniform_initializer(-0.001, 0.001, seed=self.config.RANDOM_SEED)
        item_feature = tf.Variable(tf.random_uniform([self.config.ITEM_NUMBER, self.config.LATENT_FACTOR],
                                                     -0.001, 0.001),trainable=self.trainable,name='item_feature')
        self.item_feature = item_feature
        user_feature = tf.Variable(tf.random_uniform([self.config.USER_NUMBER, self.config.LATENT_FACTOR],
                                                     -0.001, 0.001),trainable=self.trainable,name='user_feature')
        self.user_feature = user_feature
        u_embedding = tf.nn.embedding_lookup(user_feature,self.uid)
        initial_state = tuple([u_embedding for _ in range(self.config.RNN_LAYER)])
        purchased_items = tf.reduce_mean(tf.nn.embedding_lookup(item_feature,self.purchased_items),axis=2)

        ### CNN
        window_size = self.config.window_size
        in_channels = self.config.LATENT_FACTOR
        filters,bias,cnn_outputs,valid_cnn_output = [],[],[],[]
        for ws in window_size:
            filters.append(tf.Variable(tf.random_uniform((ws,in_channels,int(self.config.LATENT_FACTOR/len(window_size))),-0.001, 0.001),trainable=True))
            bias.append(tf.Variable(np.zeros((int(self.config.LATENT_FACTOR/len(window_size)),)),trainable=True,dtype=tf.float32))
            cnn_outputs.append(tf.tanh(tf.add(tf.nn.conv1d(value=tf.transpose(purchased_items,perm=[1, 0, 2]), filters=filters[-1], stride=1, padding='SAME'),bias[-1])))
            valid_cnn_output.append(tf.transpose(cnn_outputs[-1],perm=[1, 0, 2])) # shape: [max_len,batch_size,local_out_channels]
        valid_cnn_output = tf.concat(valid_cnn_output,2)
        self.valid_cnn_output = valid_cnn_output
        self.filter = filters
        self.bias = bias
        #### CNN

        # purchased_items_time = tf.concat([purchased_items,self.purchased_time],2)
        self.rnn_outputs,rnn_state = self.build_cell(self.config.CELL_TYPE,
                                                     initializer,
                                                     self.config.RNN_HIDDEN,
                                                     valid_cnn_output,initial_state)
        rnn_out = tf.gather_nd(self.rnn_outputs, self.target_index)

        ### for attention
        self.categories = tf.constant(self._load_category())
        self.cycle_matrix = tf.Variable(tf.zeros([self.config.CATE_NUMBER, self.config.CATE_NUMBER])+self.config.max_cycle,trainable=self.trainable,name='cate_cycle')
        seq_mask = tf.cast(tf.reduce_mean(tf.transpose(self.purchased_items,[1,0,2]),2),tf.bool)
        seq = tf.transpose(self.rnn_outputs,[1,0,2])
        time = tf.transpose(self.purchased_time,[1,0,2])
        purchased_items4atten = tf.transpose(self.purchased_items,[1,0,2])
        p_atten,self.sim_matrix_p,self.time_p,self.atten_p,self.d_ij_p,self.score_p,self.ij_p,self.d_time_p = time_attention(tf.nn.embedding_lookup(item_feature, self.target_sample),seq,seq_mask,
                                 self.target_sample,purchased_items4atten,self.categories,self.cycle_matrix,time)
        self.p_atten = p_atten

        #####################
        last_layer = [tf.Variable(tf.random_uniform((self.config.ITEM_NUMBER,
                                                    self.config.LATENT_FACTOR), -1.0, 0.0),
                                 name='last_layer_'+str(i), trainable=True) for i in range(3)]
        pfeature = [tf.nn.embedding_lookup(last_layer[i], self.target_sample) for i in range(3)]

        w1,w2,w3 = 0.2,0.2,0.2
        self.pscore = tf.reshape(w1*non_linear(tf.reduce_sum(tf.multiply(u_embedding,pfeature[0]),axis=1))+\
                      w2*non_linear(tf.reduce_sum(tf.multiply(rnn_out,pfeature[1]),axis=1))+\
                      w3*non_linear(tf.reduce_sum(tf.multiply(p_atten,pfeature[2]),axis=1)),(-1,))

        expand_rnn_out = tf.reshape(tf.tile(rnn_out, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))
        expand_user = tf.reshape(tf.tile(u_embedding, [1, self.config.ITEM_NUMBER]), (self.config.ITEM_NUMBER, -1))

        atten_f = time_attention_all(item_feature, seq, seq_mask, tf.range(self.config.ITEM_NUMBER),
                                                                   purchased_items4atten,self.categories,self.cycle_matrix,time)
        self.all_score = tf.reduce_sum(w1*non_linear(tf.multiply(expand_user, last_layer[0]))+\
                                       w2*non_linear(tf.multiply(expand_rnn_out, last_layer[1]))+ \
                                       w3*non_linear(tf.multiply(atten_f, last_layer[2])),1)
        self.regularizer = tf.nn.l2_loss(item_feature)+\
                           tf.nn.l2_loss(user_feature)+\
                           tf.nn.l2_loss(last_layer[0])+tf.nn.l2_loss(last_layer[1])+tf.nn.l2_loss(last_layer[2])

    def _create_optimizer(self):
        self.loss = tf.log(tf.reduce_sum(self.pscore)) - tf.multiply(tf.reduce_sum(self.all_score,axis=-1),self.delta_t)

        optimizer = self.config.optimizer_name(self.config.learning_rate)
        gvs = optimizer.compute_gradients(self.loss)
        capped_gvs = [(tf.clip_by_value(grad, -1.0, 1.0), var) for grad, var in gvs]
        # n_gvs = []
        # for grad,var in capped_gvs:
        #     if var.name == self.cycle_matrix.name:
        #         print(var.name)
        #         print(self.cycle_matrix.name)
        #         n_gvs.append((grad,var))
        #     else:
        #         n_gvs.append((grad,var))
        self.optimizer = optimizer.apply_gradients(capped_gvs, global_step=self.global_step)
        # self.optimizer = tf.train.AdamOptimizer(self.config.learning_rate).minimize(self.loss)

    def debug_model(self,sess,data):
        feed_dicts = self._get_feed_dict("optimizer",data)
        try:
            temp = self.old - sess.run(self.cycle_matrix, feed_dicts)
            print(temp[np.nonzero(temp)])
            print(temp.shape)
        except:
            pass
        self.old = sess.run(self.cycle_matrix, feed_dicts)
        ipdb.set_trace()
        sess.run(self.d_time_p,feed_dicts)
        return 0.0
        sess.run(self.score_p,feed_dicts)
        sess.run(self.optimizer,feed_dicts)
        sess.run(self.d_ij_p,feed_dicts)
        sess.run(self.d_time_p,feed_dicts)
        sess.run(self.positve_sample,feed_dicts)
        sess.run(tf.nn.embedding_lookup(self.categories,self.positve_sample),feed_dicts)
        sess.run(tf.sigmoid(tf.nn.relu(self.d_ij_p-self.time_p)/max_d_ij),feed_dicts)
        # return sess.run([self.loss,self.optimizer],feed_dicts)[0]

    def save_d_matrix(self,sess):
        d = sess.run(self.cycle_matrix)
        res = []
        with open("cycle_"+self.config.model+".txt","w") as f:
            for i in range(d.shape[0]):
                for j in range(d.shape[1]):
                    res.append(str(d[i][j]))
                res.append("\n")
            f.writelines("\t".join(res))





