#!/usr/bin/env bash
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/*.md";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/*.sh";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/zplot";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/utils";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/train";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/metrics";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/log";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/function_approximation";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/datasource";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/*.log";
rm -r "/home/zoulixin/PyCharmProject/next_basket_cycle/*.py";


scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/*.md" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/*.sh" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/zplot" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/utils" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/train" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/metrics" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/log/*" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/function_approximation" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/datasource" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/*.log" ./;
scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/*.py" ./;
#scp -r "zoulixin@166.111.71.29:/home/zoulixin/PyCharmProject/next_basket_cycle/data" ./;
mkdir saved_model

while 1
do
~/pyenv3/bin/python plot_cycle.py;
sleep 30s;
done