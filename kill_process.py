#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: kill_process
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/22,11:25 AM
#==================================

import numpy as np
import ipdb
import psutil
import os
pids = psutil.pids()
for pid in pids:
    p = psutil.Process(pid)
    if p.name().__contains__("python3"):
        # print(pid,p.name(),p.cmdline())
        temp = p.cmdline()
        if str(temp[5]).__contains__("mf_bpr"):
            print(temp[5])
            os.system("kill -9 "+str(pid))