import pandas as pd
import numpy as np
import tensorflow as tf
from log import setup_logger
import time
from tensorflow.python import debug as tf_debug
import pdb
import os


class DataGeneration:
    def __init__(self,
                 train_records,
                 test_records,
                 meta_file,
                 logger=None):
        self.train_records=train_records
        self.test_records=test_records
        self.logger=logger

        metadata=pd.read_csv(meta_file)
        self.user_num=metadata['user_num'][0]
        self.item_num=metadata['item_num'][0]
        self.pos_num=metadata['pos_num'][0]

        return

    @staticmethod
    def _decode(serialized_example):
        parse_dics = {'user_id': tf.FixedLenFeature([], tf.int64),
                      'positive_ids': tf.VarLenFeature(dtype=tf.int64),
                      'targets': tf.VarLenFeature(dtype=tf.int64),
                      'item_seq': tf.VarLenFeature(dtype=tf.int64),
                      'seq_len': tf.FixedLenFeature([], tf.int64)}
        features = tf.parse_single_example(serialized_example, features=parse_dics)

        features['positive_ids'] = tf.sparse_tensor_to_dense(features['positive_ids'])
        features['targets'] = tf.sparse_tensor_to_dense(features['targets'])
        features['item_seq'] = tf.sparse_tensor_to_dense(features['item_seq'])

        user_id = tf.cast(features['user_id'], tf.int32)
        positive_ids = tf.cast(features['positive_ids'], tf.int32)
        targets = tf.cast(features['targets'], tf.int32)
        item_seq = tf.cast(features['item_seq'], tf.int32)
        seq_len=tf.cast(features['seq_len'], tf.int32)

        return user_id, item_seq, positive_ids, targets, seq_len

    def train_inputs(self,batch_size):
        dataset=tf.data.TFRecordDataset(self.train_records)
        dataset=dataset.map(self._decode)
        dataset=dataset.shuffle(2100000)
        dataset=dataset.padded_batch(batch_size,padded_shapes=([],[None],[None],[None],[]))
        # dataset=dataset.repeat()
        self.train_dataset=dataset

        iterator=self.train_dataset.make_initializable_iterator()
        return iterator

    def test_inputs(self,batch_size):
        dataset=tf.data.TFRecordDataset(self.test_records)
        dataset=dataset.map(self._decode)
        dataset=dataset.padded_batch(batch_size,padded_shapes=([],[None],[None],[None],[]))
        # dataset=dataset.repeat()
        dataset = dataset.prefetch(510000)
        self.test_dataset=dataset

        iterator=self.test_dataset.make_initializable_iterator()
        return iterator

class RCNN4Rec:
    def __init__(self,
                 train_records,
                 test_records,
                 meta_file,
                 model_name,
                 logger=None,
                 ckpt_steps=1000,
                 ckpt_dir='./model/',
                 ckpt_file=None,
                 tbdir='./tensorboard/',
                 load_model=False,
                 K=(5,),
                 learning_rate=0.0005,
                 decay_rate=0.9,
                 decay_steps=1000,
                 batch_size=50,
                 epoch=20,
                 neg_num=3,
                 local_win_size=3,
                 local_cnn_layers=1,
                 local_out_channels=(150,),
                 dynamic_dim=150,
                 embed_dim=100,
                 l2_norm=0.0005,
                 keep_prob=1.0,
                 predict_phase=False):
        self.model_name=model_name
        self.logger=logger
        self.ckpt_steps=ckpt_steps
        self.ckpt_dir=ckpt_dir
        self.ckpt_file=ckpt_file
        self.tbdir=tbdir
        self.load_model=load_model
        self.K=K
        self.init_learning_rate=learning_rate
        self.decay_rate=decay_rate
        self.decay_steps=decay_steps
        self.batch_size=batch_size
        self.epoch=epoch
        self.neg_num=neg_num
        self.local_win_size=local_win_size
        self.local_cnn_layers=local_cnn_layers
        self.local_out_channels=local_out_channels
        self.rnn_hidden_dim=local_out_channels[-1]/2
        self.dynamic_dim=dynamic_dim
        self.embed_dim = embed_dim
        self.user_embed_dim=50
        self.l2_norm=l2_norm
        self.train_keep_prob=keep_prob
        self.predict_phase=predict_phase

        self.logger.info('\n************* hyper parameters ***************'+
                         '\npredict phase=%s'%str(self.predict_phase)+
                         '\nload model=%s'%str(self.load_model)+
                         '\nK=%s'%str(K)+
                         '\ninit learning rate=%.5f'%learning_rate+
                         '\nlearning rate decay rate=%.5f'%decay_rate+
                         '\nlearning rate decay steps=%d'%decay_steps+
                         '\nbatch size=%d'%batch_size+
                         '\nepoch=%d'%epoch+
                         '\nnegative number=%d'%neg_num+
                         '\nlocal window size=%d'%local_win_size+
                         '\nlocal_cnn layers=%d'%local_cnn_layers+
                         '\nlocal out channels=%s'%str(local_out_channels)+
                         '\ndynamic dimension=%d'%dynamic_dim+
                         '\nembedding dimension=%d'%embed_dim+
                         '\nl2 norm=%.6f'%l2_norm+
                         '\nkeep_prob=%.6f'%keep_prob+
                         '\n**********************************************')

        self.dg=DataGeneration(train_records,test_records,meta_file,logger)
        self.sess=None
        self.writer=None
        self.saver=None

        self.logger.info('\n********* Data initialization finish **********'+
                         '\ntrain file path: %s'%train_records+
                         '\nvalidation file path: %s'%test_records+
                         '\ncheckpoint directory: %s'%self.ckpt_dir+
                         '\nitem number: %d'%self.dg.item_num+
                         '\nposition number: %d'%self.dg.pos_num+
                         '\nuser number: %d'%self.dg.user_num+
                         '\n**********************************************')

        tf.reset_default_graph()

        # because zero is used for padding
        zero_mask = np.ones([self.dg.item_num])
        zero_mask[0] = 0
        self.zero_mask = tf.constant(zero_mask, dtype=tf.float32)

        # define inputs tensors
        self.train_iter=self.dg.train_inputs(self.batch_size)
        self.test_iter=self.dg.test_inputs(self.batch_size)
        self.iter_handle=tf.placeholder(tf.string,shape=[],name='iter_handle')
        self.iterator=tf.data.Iterator.from_string_handle(self.iter_handle,self.dg.train_dataset.output_types,self.dg.train_dataset.output_shapes)
        self.user_id,self.item_seq,self.positive_ids,self.targets,self.seq_len=self.iterator.get_next()

        # preprocess data
        # compute item position
        self.item_pos=tf.range(tf.reduce_max(self.seq_len))
        # compute sequence length mask
        self.seq_len_mask=tf.cast(tf.not_equal(self.item_seq,0),tf.float32)
        # compute labels
        self.labels = tf.reduce_sum(tf.one_hot(self.positive_ids, self.dg.item_num, on_value=1.0, off_value=0.0), axis=1)
        self.labels = self.labels * self.zero_mask

        # compute targets
        self.targets_mask=tf.reduce_sum(tf.one_hot(self.targets,self.dg.item_num,on_value=1.0,off_value=0.0),axis=1)
        self.targets_mask = self.targets_mask * self.zero_mask

        # evaluation metrics
        self.recall_t=tf.placeholder(dtype=tf.float32,shape=[],name='recall')
        self.precision_t=tf.placeholder(dtype=tf.float32,shape=[],name='precision')
        self.mrr_t=tf.placeholder(dtype=tf.float32,shape=[],name='mrr')
        self.keep_prob=tf.placeholder(dtype=tf.float32,shape=[],name='keep_prob')

        self.global_step=tf.get_variable('global_step',shape=[],dtype=tf.int32,trainable=False)
        self.learning_rate=tf.train.exponential_decay(self.init_learning_rate,self.global_step,self.decay_steps,self.decay_rate,name='learning_rate')
        # embedding matrix initialization
        embed_initializer=tf.random_normal_initializer(stddev=0.1)
        self.item_embedding_matrix = tf.get_variable('item_embedding_matrix',shape=[self.dg.item_num, self.embed_dim],initializer=embed_initializer)  # shape: [item_num,embed_dim]
        # self.pos_embedding_matrix=tf.get_variable('pos_embedding_maxtrix',shape=[self.dg.pos_num,self.embed_dim],initializer=embed_initializer) # shape: [pos_num,embed_dim]
        self.user_embedding_matrix=tf.get_variable('user_embedding_matrix',shape=[self.dg.user_num,self.user_embed_dim],initializer=embed_initializer)

        # cnn params initialization
        self.local_cnn_filters=[]
        self.local_cnn_bias=[]
        for i in range(self.local_cnn_layers):
            in_channels=self.embed_dim if i==0 else self.local_out_channels[i-1]/2
            filters_initializer=tf.random_normal_initializer(stddev=np.sqrt(4.0/(self.local_win_size*in_channels)))
            filters=tf.get_variable('local_cnn_filters_%d'%i,shape=[self.local_win_size,in_channels,self.local_out_channels[i]],initializer=filters_initializer)
            import ipdb
            ipdb.set_trace()
            bias=tf.get_variable('local_cnn_bias_%d'%i,shape=[self.local_out_channels[i]],initializer=tf.zeros_initializer)
            self.local_cnn_filters.append(filters)
            self.local_cnn_bias.append(bias)

        # rnn params initialization
        self.rnn_cell=tf.nn.rnn_cell.GRUCell(self.rnn_hidden_dim)
        # self.rnn_cell=tf.nn.rnn_cell.DropoutWrapper(self.rnn_cell,output_keep_prob=self.keep_prob)

        # dynamic params initialization
        weights_dynamic_initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0/self.local_out_channels[-1]))
        self.weights_dynamic=tf.get_variable('weights_dynamic',shape=[self.local_out_channels[-1],self.dynamic_dim],initializer=weights_dynamic_initializer)
        self.bias_dynamic=tf.get_variable('bias_dynamic',shape=[self.dynamic_dim],initializer=tf.zeros_initializer)

        # output layer params initialization
        weights_out_initializer=tf.random_normal_initializer(stddev=np.sqrt(1.0/(self.dynamic_dim+self.user_embed_dim)))
        self.weights_out=tf.get_variable('weights_out',shape=[self.dynamic_dim+self.user_embed_dim,self.dg.item_num],initializer=weights_out_initializer)    # shape :[embed_dim.item_num]
        self.bias_out=tf.get_variable('bias_out',shape=[self.dg.item_num],initializer=tf.zeros_initializer)

        self.build_model()

        return

    def build_model(self):
        self.logger.info('building model...')

        self.user_embed=tf.nn.embedding_lookup(self.user_embedding_matrix,self.user_id,name='user_embed')   # shape: [batch_size,embed_dim]
        self.item_seq_embed = tf.nn.embedding_lookup(self.item_embedding_matrix, self.item_seq,name='item_seq_embed')  # shape: [batch_size,max_len,embed_dim]

        # compute local states
        local_cnn_inputs=self.item_seq_embed
        for i in range(self.local_cnn_layers):
            cnn_outputs=tf.nn.conv1d(value=local_cnn_inputs,filters=self.local_cnn_filters[i],stride=1,padding='SAME',name='local_cnn_out_%d'%i)  # shape: [batch_size,max_len,local_out_channels[i]]
            cnn_outputs=tf.add(cnn_outputs,self.local_cnn_bias[i],name='local_cnn_out_with_bias_%d'%i)  # shape: [batch_size,max_len,local_out_channels[i]]
            # compute glu
            cnn_outputs_A,cnn_outputs_B=tf.split(cnn_outputs,2,axis=2,name='local_cnn_outputs_split_%d'%i)    # shape: [batch_size,max_len,local_out_channels[i]/2]
            gates=tf.sigmoid(cnn_outputs_B,name='local_gates_%d'%i)
            self.local_gated_outputs=tf.multiply(cnn_outputs_A,gates,name='local_gated_outputs_%d'%i)
            local_cnn_inputs=self.local_gated_outputs

        seq_len_mask=tf.expand_dims(self.seq_len_mask,axis=2)  # shape: [batch_size,max_len,1]
        self.local_valid_outputs=tf.multiply(seq_len_mask,self.local_gated_outputs,name='local_valid_outputs')    # shape: [batch_size,max_len,local_out_channels[i]/2]

        # compute global states
        rnn_init_state=self.rnn_cell.zero_state(tf.shape(self.item_seq)[0],tf.float32)
        seq_len=tf.cast(tf.reduce_sum(self.seq_len_mask,axis=1),dtype=tf.int32,name='seq_len')
        rnn_out_states,_=tf.nn.dynamic_rnn(self.rnn_cell,self.item_seq_embed,seq_len,rnn_init_state)    # shape: [batch_size,max_len,rnn_hidden_dim]
        self.global_state=tf.gather_nd(rnn_out_states,tf.stack([tf.range(tf.shape(self.item_seq)[0]),seq_len-1],axis=1),name='global_state')    # shape: [batch_size,rnn_hidden_dim]

        # compute attention
        global_state=tf.expand_dims(self.global_state,axis=1)
        dot_product=tf.reduce_sum(tf.multiply(global_state,self.local_valid_outputs),axis=2)
        softmask=tf.ones_like(dot_product)*(-2**32+1)
        cond=tf.cast(self.seq_len_mask,tf.bool)
        dot_product=tf.where(cond,dot_product,softmask)
        self.attention=tf.nn.softmax(dot_product,dim=1,name='attention')    # shape: [batch_size,max_len]

        # compute attentive states
        attention=tf.expand_dims(self.attention,axis=2)
        attentive_outputs=tf.multiply(attention,self.local_valid_outputs,name='attentive_outputs')      # shape: [batch_size,max_len,local_out_channels[i]/2]
        self.attentive_state=tf.reduce_sum(attentive_outputs,axis=1,name='attentive_state')     # shape: [batch_size,local_out_channels[i]/2]

        # concat global state and local state
        dynamic_state=tf.concat([self.global_state,self.attentive_state],axis=1)   # shape: [batch_size,local_out_channels[i]/2+hidden_dim]
        dynamic_state=tf.nn.dropout(dynamic_state,keep_prob=self.keep_prob)
        dynamic_state=tf.matmul(dynamic_state,self.weights_dynamic)
        self.dynamic_state=tf.add(dynamic_state,self.bias_dynamic,name='dynamic_state')     # shape: [batch_size,dynamic_dim]
        self.dynamic_state=tf.nn.relu(self.dynamic_state)
        # self.dynamic_state=tf.nn.dropout(dynamic_state,keep_prob=self.keep_prob,name='dynamic_dropout')

        # concat dynamic state and static state
        self.comp_state=tf.concat([self.dynamic_state,self.user_embed],axis=1)
        # self.comp_state=tf.nn.dropout(self.comp_state,keep_prob=self.keep_prob)

        # compute cross entropy loss
        self.logits=tf.add(tf.matmul(self.comp_state,self.weights_out),self.bias_out,name='logits')     # shape: [batch_size,item_num]
        self.outputs=tf.nn.sigmoid_cross_entropy_with_logits(labels=self.labels,logits=self.logits,name='outputs')
        self.target_outputs=tf.multiply(self.targets_mask,self.outputs,name='target_outputs')
        self.cross_entropy_loss=tf.reduce_mean(tf.reduce_sum(self.target_outputs,axis=1),name='loss')

        for v in tf.trainable_variables():
            if 'bias' not in v.name.lower():
                self.cross_entropy_loss+=self.l2_norm*tf.nn.l2_loss(v)

        # compute bpr loss
        # positive_mask=tf.not_equal(self.labels,0)
        # positive_scores = tf.boolean_mask(self.logits,positive_mask)
        # positive_scores = tf.expand_dims(positive_scores, axis=1)
        # positive_scores = tf.tile(positive_scores, [1, self.neg_num])
        #
        # negative_mask=tf.logical_and(tf.equal(self.labels,0),tf.not_equal(self.targets_mask,0))
        # negative_scores = tf.boolean_mask(self.logits,negative_mask)
        # negative_scores = tf.reshape(negative_scores, shape=[-1, self.neg_num])
        #
        # self.bpr_loss = -tf.reduce_mean(tf.log(tf.sigmoid(positive_scores - negative_scores)))
        #
        # optimizer
        self.cross_entropy_loss_optimizer=tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.cross_entropy_loss,name='cross_entropy_update',global_step=self.global_step)
        # self.bpr_loss_optimizer=tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.bpr_loss,name='bpr_update',global_step=self.global_step)

        return

    def pad_seq(self,item_seq):
        maxlen=max(map(len,item_seq))
        item_seq=list(map(lambda x:x+[0]*(maxlen-len(x)),item_seq))
        return item_seq

    def train_model(self):
        self.logger.info('training model...')
        self.saver=tf.train.Saver(max_to_keep=20)
        config=tf.ConfigProto()
        config.gpu_options.allow_growth=True
        with tf.Session(config=config) as self.sess:
            # self.sess=tf_debug.LocalCLIDebugWrapperSession(self.sess)
            if self.load_model:
                self.restore()
            else:
                self.sess.run(tf.global_variables_initializer())
            self.writer=tf.summary.FileWriter(self.tbdir)

            tf.summary.scalar('learning_rate',self.learning_rate)
            tf.summary.scalar('train_loss',self.cross_entropy_loss)
            for v in tf.trainable_variables():
                tf.summary.histogram(v.name,v)
            self.train_summs=tf.summary.merge_all()

            train_recall_summ=tf.summary.scalar('train_recall',self.recall_t)
            train_precision_summ=tf.summary.scalar('train_precision',self.precision_t)
            train_mrr_summ=tf.summary.scalar('train_mrr',self.mrr_t)
            self.train_metrics_summ=tf.summary.merge([train_recall_summ,train_precision_summ,train_mrr_summ])

            test_recall_summ=tf.summary.scalar('test_recall',self.recall_t)
            test_precision_summ=tf.summary.scalar('test_precision',self.precision_t)
            test_mrr_summ=tf.summary.scalar('test_mrr',self.mrr_t)
            self.test_metrics_summ=tf.summary.merge([test_recall_summ,test_precision_summ,test_mrr_summ])

            total_recalls = [[] for i in range(len(self.K))]
            total_precisions = [[] for i in range(len(self.K))]
            total_mrrs = [[] for i in range(len(self.K))]

            self.train_handle=self.sess.run(self.train_iter.string_handle())
            self.test_handle=self.sess.run(self.test_iter.string_handle())

            for e in range(self.epoch):
                t1=time.time()
                total_loss=[]
                self.sess.run(self.train_iter.initializer)
                while True:
                    try:
                        fetches=[self.logits,
                                 self.labels,
                                 self.item_seq,
                                 self.cross_entropy_loss,
                                 self.train_summs,
                                 self.global_step,
                                 self.cross_entropy_loss_optimizer]
                        feed_dict={self.iter_handle:self.train_handle,self.keep_prob:self.train_keep_prob}
                        scores,labels,item_seq,loss,summs,global_step,_=self.sess.run(fetches,feed_dict=feed_dict)

                        # compute metrics
                        total_loss.append(loss)
                        topks = self.topk_index(scores, self.K,item_seq)
                        recalls = self.recall(topks, labels)
                        precisions = self.precision(topks, labels)
                        mrrs = self.MRR(topks, labels)
                        for i in range(len(self.K)):
                            total_recalls[i] += recalls[i]
                            total_precisions[i] += precisions[i]
                            total_mrrs[i] += mrrs[i]

                        if global_step % self.ckpt_steps==0:
                            self.logger.info('saving checkpoint...')
                            self.saver.save(self.sess,self.ckpt_dir+self.model_name,global_step=global_step)

                        if global_step%50==0:
                            self.logger.info('training: epoch %d, loss:%.6f'%(e,loss))
                            self.writer.add_summary(summs,global_step)

                    except tf.errors.OutOfRangeError:
                        break

                        # if global_step%1000==0:
                t2=time.time()
                self.logger.info('Epoch %d finish: mean loss=%.6f, cost time=%.6f seconds'%(e,sum(total_loss)/len(total_loss),t2-t1))

                mean_recall = np.mean(total_recalls, axis=1)
                mean_precision = np.mean(total_precisions, axis=1)
                mean_mrr = np.mean(total_mrrs, axis=1)
                self.logger.info('\n********* train metrics **********' +
                                 '\nK=%s' % str(self.K) +
                                 '\nmean recall=%s' % str(mean_recall) +
                                 '\nmean precision=%s' % str(mean_precision) +
                                 '\nmean mrr=%s' % str(mean_mrr))
                total_recalls = [[] for i in range(len(self.K))]
                total_precisions = [[] for i in range(len(self.K))]
                total_mrrs = [[] for i in range(len(self.K))]

                feed_dict={self.recall_t:mean_recall[1],
                           self.precision_t:mean_precision[1],
                           self.mrr_t:mean_mrr[1]}
                train_metrics_summ,global_step=self.sess.run([self.train_metrics_summ,self.global_step],feed_dict=feed_dict)
                self.writer.add_summary(train_metrics_summ,global_step)
                self.evaluate()

        return

    def recall(self,topks,labels):
        recalls=[]
        for k in self.K:
            recall_k=[]
            tk=topks[:,-k:]
            for i in range(len(tk)):
                r=np.sum(labels[i][tk[i]])/np.sum(labels[i])
                recall_k.append(r)

            recalls.append(recall_k)
        return recalls

    def precision(self,topks,labels):
        precisions=[]
        for k in self.K:
            prec_k=[]
            tk=topks[:,-k:]
            for i in range(len(tk)):
                p=np.sum(labels[i][tk[i]])/k
                prec_k.append(p)

            precisions.append(prec_k)

        return precisions

    def MRR(self,topks,labels):
        mrrs=[]
        for k in self.K:
            mrr_k=[]
            tk=topks[:,-k:]
            for i in range(len(tk)):
                mrr=np.sum(labels[i][tk[i]]/np.arange(k,0,-1))/np.sum(labels[i])
                mrr_k.append(mrr)

            mrrs.append(mrr_k)
        return mrrs

    def topk_index(self,scores,K,item_seq):
        # for i in range(len(scores)):
        #     scores[i][item_seq[i]]=-np.inf
        sorted_idxes=np.argsort(scores)
        return sorted_idxes[:,min(-np.array(K)):]

    def restore(self):
        if self.ckpt_file:
            ckptf=self.ckpt_file
        else:
            ckptf=tf.train.latest_checkpoint(self.ckpt_dir)
        if not self.sess:
            self.logger.error('Not a tensorflow session currently')
            return
        if not self.saver:
            self.logger.error('Not a saver currently')
            return
        if not ckptf:
            self.logger.error('not found checkpoint file')
            return

        self.saver.restore(self.sess,ckptf)

    def predict(self):
        self.saver=tf.train.Saver()
        config=tf.ConfigProto()
        config.gpu_options.allow_growth=True
        with tf.Session(config=config) as self.sess:
            self.test_handle=self.sess.run(self.test_iter.string_handle())
            self.restore()
            self.evaluate()

    def evaluate(self):
        self.logger.info('evaluation begin...')
        total_recalls=[[] for i in range(len(self.K))]
        total_precisions=[[] for i in range(len(self.K))]
        total_mrrs=[[] for i in range(len(self.K))]
        total_f1s=[[] for i in range(len(self.K))]

        self.sess.run(self.test_iter.initializer)
        while True:
            try:
                fetches=[self.logits,self.labels,self.item_seq]
                feed_dict={self.iter_handle:self.test_handle,self.keep_prob:1.0}
                scores,labels,item_seq=self.sess.run(fetches,feed_dict=feed_dict)

                topks=self.topk_index(scores,self.K,item_seq)
                recalls=self.recall(topks,labels)
                precisions=self.precision(topks,labels)
                mrrs=self.MRR(topks,labels)

                for i in range(len(self.K)):
                    total_recalls[i]+=recalls[i]
                    total_precisions[i]+=precisions[i]
                    total_mrrs[i]+=mrrs[i]
                    r_arr=np.array(recalls[i])
                    p_arr=np.array(precisions[i])
                    total_f1s[i]+=(2*r_arr*p_arr/(r_arr+p_arr+1e-8)).tolist()
            except tf.errors.OutOfRangeError:
                break

        mean_recall=np.mean(total_recalls,axis=1)
        mean_precision=np.mean(total_precisions,axis=1)
        mean_mrr=np.mean(total_mrrs,axis=1)
        mean_f1s=np.mean(total_f1s,axis=1)

        if self.writer:
            feed_dict={self.recall_t:mean_recall[1],
                       self.precision_t:mean_precision[1],
                       self.mrr_t:mean_mrr[1]}
            global_step,summs=self.sess.run([self.global_step,self.test_metrics_summ],feed_dict=feed_dict)
            self.writer.add_summary(summs,global_step)

        self.logger.info('\n********* Evaluation finish **********'+
                         '\nK=%s'%str(self.K)+
                         '\nmean recall=%s'%str(mean_recall)+
                         '\nmean precision=%s'%str(mean_precision)+
                         '\nmean f1-scores=%s'%str(mean_f1s)+
                         '\nmean mrr=%s'%str(mean_mrr))

        return


tf.app.flags.DEFINE_boolean('predict_phase',False,'prediction phase')
tf.app.flags.DEFINE_string('predict_file',None,'predict file')
tf.app.flags.DEFINE_string('ckpt_file',None,'checkpoint file')
tf.app.flags.DEFINE_string('model_name',None,'model name')
tf.app.flags.DEFINE_string('dataset','ml1m','dataset')
tf.app.flags.DEFINE_integer('ckpt_steps',500,'tensorflow checkpoint steps')
tf.app.flags.DEFINE_boolean('load_model',False,'Load model from checkpoint directory or not')
tf.app.flags.DEFINE_string('K','(1,5,10,20)','top k')
tf.app.flags.DEFINE_float('lr',0.004,'learning rate')
tf.app.flags.DEFINE_float('decay_rate',0.85,'learning rate decay rate')
tf.app.flags.DEFINE_integer('decay_steps',3000,'learning rate decay steps')
tf.app.flags.DEFINE_integer('batch_size',512,'batch size')
tf.app.flags.DEFINE_integer('epoch',17,'epoch')
tf.app.flags.DEFINE_integer('neg_num',3,'negative sample number')
tf.app.flags.DEFINE_integer('local_win_size',3,'local cnn window size')
tf.app.flags.DEFINE_integer('local_cnn_layers',1,'local cnn layer number')
tf.app.flags.DEFINE_string('local_out_channels','(100,)','local cnn out channels')
tf.app.flags.DEFINE_integer('dynamic_dim',100,'dynamic dimension')
tf.app.flags.DEFINE_integer('embed_dim',100,'embedding dimensions')
tf.app.flags.DEFINE_float('l2_norm',0.00003,'l2 normalization')
tf.app.flags.DEFINE_float('keep_prob',1.0,'keep prob')

FLAGS=tf.app.flags.FLAGS

def main(_):
    predict_phase=FLAGS.predict_phase
    predict_file=FLAGS.predict_file

    l2_norm=FLAGS.l2_norm
    decay_rate=FLAGS.decay_rate
    decay_steps=FLAGS.decay_steps
    lr=FLAGS.lr
    model_name=FLAGS.model_name

    if not model_name:
        model_name='%.5f_%.5f_%.5f'%(l2_norm,decay_rate,lr)

    dataset=FLAGS.dataset
    logdir='./log/%s/'%dataset
    train_file='./dataset/%s/train/train_data.tfrecord'%dataset
    if predict_phase:
        test_file=predict_file
    else:
        test_file='./dataset/%s/train/test_data.tfrecord'%dataset
    meta_file='./dataset/%s/metadata.csv'%dataset
    ckpt_dir='./checkpoint/%s/%s/'%(dataset,model_name)
    tbdir='./tensorboard/%s/%s/'%(dataset,model_name)
    ckpt_file=FLAGS.ckpt_file
    ckpt_steps=FLAGS.ckpt_steps
    load_model=FLAGS.load_model
    K=eval(FLAGS.K)
    batch_size=FLAGS.batch_size
    epoch=FLAGS.epoch
    neg_num=FLAGS.neg_num
    local_win_size=FLAGS.local_win_size
    local_cnn_layers=FLAGS.local_cnn_layers
    local_out_channels=eval(FLAGS.local_out_channels)
    dynamic_dim=FLAGS.dynamic_dim
    embed_dim=FLAGS.embed_dim
    keep_prob=FLAGS.keep_prob

    if predict_phase:
        log_file=logdir+model_name+'.test.log'
    else:
        log_file=logdir+model_name+'.train.log'

    logger=setup_logger('cnn',log_file)

    model=RCNN4Rec(train_file,
                  test_file,
                  meta_file,
                  model_name,
                  logger,
                  ckpt_steps,
                  ckpt_dir,
                  ckpt_file,
                  tbdir,
                  load_model,
                  K,
                  lr,
                  decay_rate,
                  decay_steps,
                  batch_size,
                  epoch,
                  neg_num,
                  local_win_size,
                  local_cnn_layers,
                  local_out_channels,
                  dynamic_dim,
                  embed_dim,
                  l2_norm,
                  keep_prob,
                  predict_phase)
    if not predict_phase:
        model.train_model()
    else:
        model.predict()

if __name__ == '__main__':
    tf.app.run()
