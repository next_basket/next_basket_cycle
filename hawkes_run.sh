#!/usr/bin/env bash

#!/usr/bin/env bash
declare -i BATCH_SIZE=256
#subclass number 2010
#user_number 32266
#item_number 23812
run_tafeng () {
     ~/pyenv3/bin/python3 ./train_hawkes_process.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/tafeng/tf_consuming -CATE_PATH ./data/tafeng/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 32266 -ITEM_NUMBER 23812 \
    -CATE_NUMBER 2010 -max_update $6 -max_cycle 50.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9 \
    -optimizer_name ${10}
}

name="tafeng_hawkes_cnn_rnn_attentive"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_tafeng $name training 0 ctlstm [1,3,5,10] 1000000 100 100 0.0 adagrad)
