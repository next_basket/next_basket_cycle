#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: base
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午10:38
#==================================

import numpy as np
import ipdb
from utils import zpipe
from utils.zlog import log
class datasource(object):
    def __init__(self,config):
        self.config = config
        self.load_sequtial_data()

    def load_sequtial_data(self):
        self.user_rating = {}
        self.item_cat = {}
        log.info("reading purchase file!")
        self.data = list([self.config.DATA_PATH]\
                    | zpipe.open_file \
                    | zpipe.filtering(lambda x: x.strip("\n").split("\t")) \
                    | zpipe.filtering(lambda x:[int(x[0])-1]+[(int(item.split(";")[0])-1,float(item.split(";")[-1])) for item in x[1:]]))
    @property
    def item_category(self):
        try:
            return self.category
        except:
            self.category = {}
            with open(self.config.CATE_PATH,"r") as f:
                for line in f.readlines():
                    line = line.strip("\n").split("\t")
                    self.category[int(line[0])-1] = int(line[1])-1
            return self.category

    def get_training_data(self):
        np.random.shuffle(self.data)
        length_range = []
        for i in range(1,301):
            length_range.append(np.asarray(range(0,i)))
        for _ in range(30000000):
            subset = np.random.choice(range(self.config.ITEM_NUMBER),(5000,))
            for ii,item in enumerate(self.data):
                if ii%5000==0:
                    print("update sample sets!")
                    subset = np.random.choice(range(self.config.ITEM_NUMBER), (5000,))
                if len(item)<=5:
                    continue
                if len(item)>=50:
                    continue
                uid = item[0]
                item = item[1:]
                split = np.random.choice(length_range[len(item)-2])
                negative_sample = np.random.choice(subset)
                while negative_sample == item[split][0]:
                    negative_sample = np.random.choice(subset)
                yield uid, item[:split], item[split], (negative_sample, item[split][1]), 1


    def get_hawkes_training_data(self):
        np.random.shuffle(self.data)
        length_range = []
        max_length = 50
        for i in range(1,max_length+1):
            length_range.append(np.asarray(range(0,i)))
        for _ in range(30000000):
            for ii,item in enumerate(self.data):
                if len(item)<=5:
                    continue
                if len(item)>=max_length:
                    continue
                uid = item[0]
                item = item[1:]
                split = np.random.choice(length_range[len(item)-2])
                yield uid, item[:split], item[split]



    def get_evaluate_data(self):
        evaluate_data = [self.data[item] for item in np.random.choice(len(self.data), (2000,),replace=False)]
        for item in evaluate_data:
            if len(item) <= 5:
                continue
            if len(item) >= 50:
                continue
            # if item[0] in range(self.config.USER_NUMBER-self.config.test_user_num,self.config.USER_NUMBER+2):
            else:
                yield  item[0],item[1:-1],item[-1] # uid,sequence,ground_truth

    def get_full_evaluate_data(self):
        # evaluate_data = [self.data[item] for item in np.random.choice(len(self.data), (20000,),replace=False)]
        np.random.shuffle(self.data)
        for item in self.data:
            if len(item)<5:
                continue
            if len(item)>300:
                continue
            yield  item[0],item[1:-1],item[-1] # uid,sequence,ground_truth


    def calculate_item_frequence(self):
        self.frequence = np.zeros((self.config.ITEM_NUMBER,))
        for item in self.data:
            item = item[1:]
            # if len(item) < 5:
            #     continue
            # if len(item) > 100:
            #     continue
            for i in item[1:-1]:
                self.frequence[i[0]] +=1
            # print(self.frequence)
            # ipdb.set_trace()

    def get_popular_vector(self):
        try:
            return self.frequence
        except:
            self.calculate_item_frequence()
            return self.frequence
