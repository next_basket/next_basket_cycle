#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: test_env
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/22,下午6:13
#==================================

import numpy as np
import ipdb


import gym
env = gym.make('CartPole-v0')
env.reset()
for _ in range(1000):
    env.render()
    ipdb.set_trace()
    env.step(env.action_space.sample())