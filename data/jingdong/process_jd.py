#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: process_jd
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/15,10:45 AM
#==================================

import numpy as np
import ipdb
from progressbar import ProgressBar
from datetime import datetime
purchase_path = "./record_all.txt"
category_path = "./productCsimple.txt"
result = "./jd_purchase"
category_result = "./category"
category_info = "./category_info"
min_number = 5
max_number = 50
def read_data(path):
    res = []
    u = {}
    i = {}
    with open(path,"r") as f:
        pbar = ProgressBar()
        for line in f.readlines():
            iid,uid,time = line.strip("\n").split("\t")
            month,day,year = [int(item)  for item in time.split("/")]
            try:
                days =(datetime(year,month,day,0,0) - datetime(2009, 1, 1, 0, 0)).days / 200.0
                res.append([int(uid), int(iid), 1, 1, 1, days])
            except:
                # print(year,month,day)
                pass


    # nres = []
    # for item in res:
    #     uid, iid,_,_,_, time = item
    #     if not uid in u:
    #         u[uid] = 1
    #     else:
    #         u[uid] += 1
    #     if not iid in i:
    #         i[iid] = 1
    #     else:
    #         i[iid] += 1
    #
    # for item in res:
    #     uid, iid, _, _, _, time = item
    #     if u[uid] >= min_number and i[iid] >= min_number:
    #             # and u[uid] <= 100 and i[iid] <= 100:
    #         nres.append(item)
    return res


def read_category(path):
    category = {}
    with open(path,"r",encoding="utf-8") as f:
        for i,line in enumerate(f.readlines()):
            # ipdb.set_trace()
            item,cat = line.strip("\n").split("\t")
            # print(i,cat)
            category[int(item)] = cat
            # if not str(cat).replace(" ","").replace(",","").isalpha():
            #     print(cat)
        # ipdb.set_trace()
    return category

item2category = read_category(category_path)
# print(item2category.values())
purchase_data = read_data(purchase_path)

uids_count = {}
iids_count = {}

for item in purchase_data:
    if item[0] in uids_count:
        uids_count[item[0]] += 1
    else:
        uids_count[item[0]] = 1
    if item[1] in iids_count:
        iids_count[item[1]] += 1
    else:
        iids_count[item[1]] = 1


c_dict = {item:i+1 for i,item in enumerate(set(item2category.values()))}


u_dict = {}
i_dict = {}
i2c_dict = {}
u_data = {}
iid2cid = {}
for item in purchase_data:
    uid,iid,_,_,_,days = item
    if uids_count[uid]>=min_number and uids_count[uid]<=max_number:pass
    else:continue
    try:
        temp = c_dict[item2category[iid]]
        # print(temp)
        if not uid in u_dict:
            if len(u_dict)>= 50000:continue
            else:
                u_dict[uid] = len(u_dict)+1
        if not iid in i_dict:
            i_dict[iid] = len(i_dict)+1
        iid2cid[i_dict[iid]] = c_dict[item2category[iid]]
        if not u_dict[uid] in u_data:
            u_data[u_dict[uid]] = [[i_dict[iid],1,1,c_dict[item2category[iid]],days]]
        else:
            u_data[u_dict[uid]].append([i_dict[iid],1,1,c_dict[item2category[iid]],days])
    except:
        # print("error",iid)
        pass
pass
with open(result, "w") as f:
    pbar = ProgressBar()
    for key, value in u_data.items():
        temp = []
        value = sorted(value, key=lambda x: x[4])
        for ii in value:
            temp.append(";".join([str(iii) for iii in ii]))
        f.writelines("\t".join([str(key)] + temp) + "\n")
    temp = [len(item) for item in u_data.values()]
    print("transaction", "sum", np.sum(temp), "mean", np.mean(temp), "density",
          np.sum(temp) / len(u_dict) / len(i_dict))

with open(category_result,"w") as f:
    for key, value in i_dict.items():
        f.writelines("\t".join([str(value)] + [str(c_dict[item2category[key]])]) + "\n")

with open(category_info,"w") as f:
    pbar = ProgressBar()
    for key, value in c_dict.items():
        f.writelines("\t".join([str(value)] + [str(key)]) + "\n")
print("user",len(u_dict))
print("item",len(i_dict))
print("category",len(c_dict))






