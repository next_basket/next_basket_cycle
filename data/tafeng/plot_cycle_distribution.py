#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: plot_cycle_distribution
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/12,下午9:37
#==================================

import numpy as np
import ipdb

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import copy as cp
import numpy as np
import ipdb
from matplotlib import pyplot as plt
from scipy import optimize
import brewer2mpl
from collections import Counter
# brewer2mpl.get_map args: set name set type number of colors
bmap = brewer2mpl.get_map('Set2','qualitative', 7)
colors = bmap.hex_colors
import matplotlib as mpl
import matplotlib.font_manager as fm
# mpl.rcParams['axes.color_cycle'] = colors
font = fm.FontProperties(size='medium')
font.set_weight('bold')
new_font = fm.FontProperties(size=15)
def plot_sub_class():
    repeats = {1: 435732, 2: 88379, 3: 26364, 4: 10593, 5: 4957, 6: 2691, 7: 1416, 8: 932, 9: 573, 10: 365, 11: 275, 12: 186, 13: 144, 14: 105, 15: 71, 16: 65, 17: 42, 18: 32, 19: 27, 20: 21, 22: 20, 23: 15, 21: 11, 26: 10, 27: 10, 25: 9, 30: 6, 24: 6, 29: 4, 37: 4, 33: 3, 31: 3, 39: 2, 34: 2, 28: 2, 35: 2, 50: 1, 78: 1, 127: 1, 66: 1, 55: 1, 44: 1, 32: 1, 62: 1, 120: 1, 38: 1, 104: 1, 57: 1, 40: 1, 58: 1}
    repeats = repeats.items()
    repeats = sorted(list(repeats),key=lambda x:x[0])
    fig = plt.figure(1, figsize=(8, 3), tight_layout={'pad': 0.2, 'w_pad': 2.0, 'h_pad': 1.0})
    plt.bar([item[0] for item in repeats],[item[1] for item in repeats])
    plt.show()
    fig.savefig('./frequency.png', dpi=fig.dpi)

