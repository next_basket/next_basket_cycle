#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: processing_tafeng_data
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/12,上午2:56
#==================================
import numpy as np
import ipdb

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import copy as cp
import numpy as np
import ipdb
from matplotlib import pyplot as plt
from scipy import optimize
import brewer2mpl
from collections import Counter
# brewer2mpl.get_map args: set name set type number of colors
bmap = brewer2mpl.get_map('Set2','qualitative', 7)
colors = bmap.hex_colors
import matplotlib as mpl
import matplotlib.font_manager as fm
# mpl.rcParams['axes.color_cycle'] = colors
font = fm.FontProperties(size='medium')
font.set_weight('bold')
new_font = fm.FontProperties(size=15)
import numpy as np
import ipdb
import datetime
from collections import Counter

elements = ["time","uid","age","area","prod_subclass","itemid","amount","asset","prices"]
def split_data(line):
    line = line.split(";")
    res = {}
    for i,item in enumerate(elements):
        res[item] = line[i]
    return res

def get_element(data={}):
    return (int(data["uid"].strip(" ")),
            int(data["itemid"].strip(" ")),
            int(data["amount"].strip(" ")),
            int(data["prices"].strip(" ")),
            datetime.datetime.strptime(data["time"], "%Y-%m-%d %H:%M:%S"),
            int(data["prod_subclass"].strip(" ")))

def detect_frequency4item(user_record):
    res = {}
    for item in user_record:
        if not item[0] in res:
            res[item[0]] = [(item[1],item[2],item[-1])]
        else:
            res[item[0]].append((item[1],item[2],item[-1]))
    frequency_items = {}
    for key,value in res.items():
        value = sorted(value,key=lambda x:x[2])
        frequency_items[key] = [len(value),[item[-2]-value[i][-2] for i,item in enumerate(value[1:])]]
    return frequency_items

def plot_time_cycle(repeats):
    fig = plt.figure(1, figsize=(8, 8), tight_layout={'pad': 0.2, 'w_pad': 2.0, 'h_pad': 1.0})
    for i,y in enumerate(repeats):
        y = [item for item in y if item>0]
        x = np.random.normal((i+1)*3,0.5,(len(y),))
        plt.plot(x,y,'o', alpha=0.3, mfc='none', markersize=3)
    plt.show()
    fig.savefig('./time_cycle.png', dpi=fig.dpi)

def save_user_record(user_dict = {},path = "./tf_consuming"):
    users,items,sub_classes = {},{},{}
    item_subclasses= {}
    with open(path,"w") as f:
        for key,value in user_dict.items():
            temp = []
            if not key in users:
                users[key] = len(users)+1
            temp.append(str(users[key]))
            random_value = np.random.uniform(0, 1, (len(value),))
            random_value = sorted(random_value)
            for iii,item in enumerate(value):
                if not item[0] in items:
                    items[item[0]] = len(items)+1
                if not item[-1] in sub_classes:
                    sub_classes[item[-1]] = len(sub_classes)+1
                item_subclasses[items[item[0]]] = sub_classes[item[-1]]
                temp.append(";".join([str(items[item[0]]),str(item[1]),str(item[2]),str(sub_classes[item[-1]]),str(((item[3]-datetime.datetime(2000, 1, 1, 0, 0)).days+random_value[iii])/200.0)]))
            f.writelines("\t".join(temp)+"\n")
    with open("category","w") as f:
        keys = item_subclasses.keys()
        keys = sorted(keys)
        for key in keys:
            f.writelines("\t".join([str(key),str(item_subclasses[key])])+"\n")
        print("subclass number",len(set(list(item_subclasses.values()))))
    print("user_number",len(users))
    print("item_number",len(items))
    temp = [len(item) for item in user_dict.values() if len(item)>=5]
    item_num = set([items[item[0]] for sublist in user_dict.values() if len(sublist)>=5 for item in sublist])
    ipdb.set_trace()
    print("transaction","user",len(temp),"subitem",len(item_num),"sum",np.sum(temp),"mean",np.mean(temp),"density",np.sum(temp)/len(users)/len(items))


with open("./tafeng",encoding="utf-8") as f:
    user_record = {}
    frequecy_count = {}
    for line in f.readlines():
        record = get_element(split_data(line))
        if not record[0] in user_record:
            user_record[record[0]] = [record[1:]]
        else:
            user_record[record[0]].append(record[1:])

    save_user_record(user_record)
    for key,value in user_record.items():
        frequency_item = detect_frequency4item(value)
        for key,value in frequency_item.items():
            if not key in frequecy_count:
                frequecy_count[key] = [value]
            else:
                frequecy_count[key].append(value)


    frequency = []
    for key,value in frequecy_count.items():
        for item in value:
            frequency.append(item[0])

    time_gap = {}
    for key,value in frequecy_count.items():
        gaps = []
        for item in value:
            gaps.extend(item[1])
        time_gap[key] = gaps
    temp = sorted(list(time_gap.values()),key=lambda x:len(x),reverse = True)
    plot_time_cycle(temp[:10])