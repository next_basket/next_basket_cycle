#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: process_tianchi
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/13,2:47 PM
#==================================

import numpy as np
import ipdb

import numpy as np
import ipdb
import ast

from progressbar import ProgressBar
from datetime import datetime
path = "./purchase.csv"
result = "./tianchi_purchase"
category_file = "./category"
min_number = 5
max_number = 50
def read_data(path):
    u = {}
    i = {}
    c = {}
    res = []
    with open(path,"r") as f:
        pbar = ProgressBar()
        for line in pbar(f.readlines()):
            t = line.strip("\t").strip("\n").split(",")
            res.append([int(t[0]),int(t[1]),int(t[2]),int(t[4])])
    # nres =[]
    # for item in res:
    #     uid,iid,cid,time = item
    #     if not uid in u: u[uid] = 1
    #     else: u[uid] += 1
    #     if not iid in i: i[iid] = 1
    #     else: i[iid] += 1
    #     if not cid in c: c[cid] = 1
    #     else: c[cid] += 1
    # for item in res:
    #     uid, iid, cid, time = item
    #     if u[uid]>=min_number and i[iid] >= min_number and c[cid]>=min_number:
    #         nres.append(item)
    return res

dataset = read_data(path)

uids_count = {}
iids_count = {}

for item in dataset:
    if item[0] in uids_count:
        uids_count[item[0]] += 1
    else:
        uids_count[item[0]] = 1
    if item[1] in iids_count:
        iids_count[item[1]] += 1
    else:
        iids_count[item[1]] = 1
# print(dataset[:10])
u_dict = {}
i_dict = {}
c_dict = {}
i2c_dict = {}
u_data = {}
for item in dataset:
    uid,iid,cid,timestamp = item
    if uids_count[uid]>=min_number:pass
    else:continue
    days = (datetime.utcfromtimestamp(float(timestamp)) -\
            datetime(2017, 11, 24, 0, 0)).total_seconds()/3600.0/24.0/50.0
    if not uid in u_dict:
        u_dict[uid] = len(u_dict)+1
    if not iid in i_dict:
        i_dict[iid] = len(i_dict)+1
    if not cid in c_dict:
        c_dict[cid] = len(c_dict)+1
    i2c_dict[i_dict[iid]] = c_dict[cid]
    if not u_dict[uid] in u_data:
        u_data[u_dict[uid]] = [[i_dict[iid],1,1,c_dict[cid],days]]
    else:
        u_data[u_dict[uid]].append([i_dict[iid],1,1,c_dict[cid],days])

print("user num",len(u_dict))
print("item num",len(i_dict))
print("cate num",len(c_dict))
temp = [len(item) for item in u_data.values()]
print("transaction", "sum", np.sum(temp), "mean", np.mean(temp), "density",
      np.sum(temp) / len(u_dict) / len(i_dict))

with open(result,"w") as f:
    for key,value in u_data.items():
        value = sorted(value, key=lambda x: x[4])
        temp = []
        for ii in value:
            temp.append(";".join([str(iii) for iii in ii]))
        f.writelines("\t".join([str(key)] + temp) + "\n")



with open(category_file, "w") as f:
    pbar = ProgressBar()
    for key, value in pbar(i2c_dict.items()):
        # print(key)
        f.writelines("\t".join([str(key)] + [str(value)]) + "\t")


