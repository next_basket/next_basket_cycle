#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: process_amazon
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/5,10:31 PM
#==================================

import numpy as np
import ipdb
import ast
from progressbar import ProgressBar
from datetime import datetime
meta_data = "./metadata.json"
# meta_data = "./temp.txt"
user_data = "./item_dedup.csv"
# user_data = "./user.txt"
result = "./amazon_review"
category_file = "./category"
item_info = "./item_info"
category_info = "./category_info"
min_purchase = 5
max_purchase = 50
def read_meta(path):
    res = {}
    with open(path,"r") as f:
        # pbar = ProgressBar()
        for line in f.readlines():
            raw_dict = ast.literal_eval(line)
            try:
                res[raw_dict["asin"]] = (raw_dict["categories"][0][0],raw_dict['title'])
            except:
                try:
                    res[raw_dict["asin"]] = (raw_dict["categories"][0][0], "None")
                except:
                    # print(line)
                    # print(raw_dict["asin"], "category does not exsit")
                    pass
        
    return res

def read_user_purchase(path,category_dict):
    res = []
    amount = 1
    price = 100
    subclass = 1
    with open(path,"r") as f:
        # user id\t item id\t amount\t price \t subclass \t days from 2000-01-01
        # pbar = ProgressBar()
        for line in f.readlines():
            uid,iid,score,time = line.split(",")
            if float(score)>=5.0 and iid in category_dict:
                days = (datetime.utcfromtimestamp(float(time))-datetime(1996, 1, 1, 0, 0)).days/200.0
                if days*200.0>6000.0:
                    res.append([uid,iid,amount,price,subclass,days])
        


    # pbar = ProgressBar()
    # for item in res:
    #     if uids[item[0]]>=min_purchase and iids[item[1]]>=min_purchase:
    #         # and uids[item[0]]<=max_purchase and iids[item[1]]<=max_purchase:
    #         res_out.append(item)
    
    return res

def reindex_data(user_data,category_data):
    uid = {}
    iid = {}
    cid = {}
    cname = {}
    user_purchase = {}
    with open(result,"w") as f:
        # pbar = ProgressBar()
        # filtering out unoften data
        uids_count = {}
        iids_count = {}

        for item in user_data:
            if item[0] in uids_count:
                uids_count[item[0]] += 1
            else:
                uids_count[item[0]] = 1
            if item[1] in iids_count:
                iids_count[item[1]] += 1
            else:
                iids_count[item[1]] = 1

        for item in user_data:
            u = item[0]
            i = item[1]
            if uids_count[u]>=min_purchase and uids_count[u]<=max_purchase: pass
            else:continue
            if not u in uid:
                if len(uid) >= 50000: continue
                uid[u] = len(uid)+1
            if not i in iid:
            #     # if len(iid) >= 200000: continue
                iid[i] = len(iid)+1
            if not uid[u] in user_purchase:
                user_purchase[uid[u]] = [[iid[i]]+item[2:]]
            else:
                user_purchase[uid[u]].append([iid[i]]+item[2:])

        pbar = ProgressBar()
        for key,value in pbar(user_purchase.items()):
            temp = []
            value = sorted(value,key=lambda x:x[4])
            for ii in value:
                temp.append(";".join([str(iii) for iii in ii]))
            f.writelines("\t".join([str(key)]+temp)+"\n")
        temp =[len(item) for item in  user_purchase.values()]
        print("transaction", "sum", np.sum(temp), "mean", np.mean(temp), "density",
              np.sum(temp) / len(uid) / len(iid))
        
    print("item number",len(iid))
    print("user number",len(uid))
    with open(category_file,"w") as f:
        # pbar = ProgressBar()
        for key,value in iid.items():
            if key in category_data:
                name = category_data[key][0]
                if not name in cname:
                    cname[name] = len(cname)+1
                f.writelines("\t".join([str(item) for item in [iid[key], cname[name]]]) + "\n")
            else:
                name = "none"
                if not name in cname:
                    cname[name] = len(cname)+1
                f.writelines("\t".join([str(item) for item in [iid[key], cname[name]]]) + "\n")
        

    with open(item_info,"w") as f:
        pbar = ProgressBar()
        for key,value in pbar(iid.items()):
            try:
                f.writelines("\t".join([str(item) for item in [value]+list(category_data[key])])+"\n")
            except:
                f.writelines("\t".join([str(item) for item in [value] + list(["none","none"])]) + "\n")
                # print("category information is is not exist!")
                pass
        
    with open(category_info,"w") as f:
        pbar = ProgressBar()
        for key,value in pbar(cname.items()):
            # print(key)
            f.writelines("\t".join([str(value)]+[key])+"\t")
        
    print("category number",len(cname))

category_data = read_meta(meta_data)
purchase_data = read_user_purchase(user_data,category_data)
# ipdb.set_trace()
reindex_data(purchase_data,category_data)