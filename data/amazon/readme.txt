Data structure for amazon

reviewerID - ID of the reviewer, e.g. A2SUAM1J3GNN3B
asin - ID of the product, e.g. 0000013714
reviewerName - name of the reviewer
helpful - helpfulness rating of the review, e.g. 2/3
reviewText - text of the review
overall - rating of the product
summary - summary of the review
unixReviewTime - time of the review (unix time)
reviewTime - time of the review (raw)

item_dedup.csv
userid, iid, rating, reviewTime

item number 220090
user number 119274
category number 68

100% |######################################################################|
transaction sum 1016358 mean 170.9313824419778 density 0.0003285473960181327
item number 520264
user number 5946
100% |######################################################################|
100% |######################################################################|
category number 75