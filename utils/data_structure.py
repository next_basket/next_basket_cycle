#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: arguments
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:7/28/18,2:03 PM
#==================================

import numpy as np
import ipdb


class random_buffer(object):
    def __init__(self,capacity):
        self.capacity = capacity
        self.buffer = []
    def put(self,data):
        self.buffer+=data
        if len(self.buffer)>self.capacity:
            self.buffer = self.buffer[len(self.buffer)-self.capacity:]
    def get(self,size):
        index = np.random.choice(range(len(self.buffer)),size)
        return [self.buffer[i] for i in index]

    def get_buffer_size(self):
        return len(self.buffer)

    def get_all(self):
        return self.buffer

    def full(self):
        if len(self.buffer) >= self.capacity:
            return True
        else:
            return False

# class fixed_buffer(random_buffer):
#     def put(self,data):
#         if len(self.buffer)>=self.capacity:
#             return
#         else:
#             super().put(data)
#
#     def get_all(self):
#         return self.buffer
#



