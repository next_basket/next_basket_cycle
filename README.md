# README #
The code for paper "Automatically Cycle Detection in Next Basket Recommendation"


### Introduction  ###

### Setup and Run ###
1. System Requirements: Ubuntu 16.04, Python 3.6
2. Dependence of python: numpy, tensorflow, tqdm, panda, scipy, matplotlib
3. Run: chmod a+x run.sh;./run.sh;

#run rnn_cnn_bpr evaluate 1 lstm

#run rnn_cnn_bpr training 2 lstm > temp.txt 2&>1 &
#run rnn_cnn_bpr evaluate 1 lstm > temp.txt 2&>1 &
#run bpr_rnn training 1 gru
#run bpr_rnn evaluate 1 gru
#run bpr_lstm training 2 lstm
#run bpr_lstm evaluate 2 lstm

#/bin/rm -r ./log/*
#/bin/rm -r ./saved_model/*
#(run naive_rnn_lstm training 0 lstm [1,3,5,10] && run cnn_rnn_user_10 training 0 ctlstm [10] && run cnn_rnn_user_all training 0 ctlstm [1,3,5,10] && run cnn_rnn_user_3 training 0 ctlstm [3]) > temp0.txt 2>&1 &
#(run naive_rnn_lstm evaluate 1 lstm [1,3,5,10] && run cnn_rnn_user_10 evaluate 1 ctlstm [10] && run cnn_rnn_user_all evaluate 1 ctlstm [1,3,5,10] && run cnn_rnn_user_3 evaluate 1 ctlstm [3]) > temp1.txt 2>&1 &
#(run cnn_rnn_old_method evaluate 3 lstm [1,3,5,10] && run naive_rnn_ctlstm training 2 ctlstm [1,3,5,10] && run cnn_rnn_user_1 training 2 ctlstm [1] && run cnn_rnn_user_5 training 2 ctlstm [5] ) > temp2.txt 2>&1 &
#(run cnn_rnn_old_method training 2 lstm [1,3,5,10] && run naive_rnn_ctlstm evaluate 3 ctlstm [1,3,5,10] && run cnn_rnn_user_1 evaluate 3 ctlstm [1] && run cnn_rnn_user_5 evaluate 3 ctlstm [5] ) > temp3.txt 2>&1 &

#(run naive_rnn_lstm_new training 2 lstm [1,3,5,10] && run naive_rnn_ctlstm_new training 2 ctlstm [1,3,5,10])  > temp2.txt 2>&1 &
#(run naive_rnn_lstm_new evaluate 3 lstm [1,3,5,10] && run naive_rnn_ctlstm_new evaluate 3 ctlstm [1,3,5,10])  > temp3.txt 2>&1 &

#/bin/rm -r ./log/*
#/bin/rm -r ./saved_model/*
#(run rnn_naive_rnn_lstm training 2 nlstm [1,3,5,10]) &
#(run rnn_naive_rnn_lstm evaluate 3 nlstm [1,3,5,10])

#(run rnn_naive_rnn_without_user_lstm training 0 nlstm [1,3,5,10]) &
#(run rnn_naive_rnn_without_user_lstm evaluate 1 nlstm [1,3,5,10])

#name="rnn_naive_rnn_ctlstm"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run $name training 2 ctlstm [1,3,5,10]) &
#(run $name evaluate 1 ctlstm [1,3,5,10])


#name="rnn_native_rnn_without_user_lstm"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run $name training 2 ctlstm [1,3,5,10]) &
#(run $name evaluate 0 ctlstm [1,3,5,10])

#name="rnn_naive_t1lstm"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run $name training 1 t1lstm [1,3,5,10]) &
#(run $name evaluate 0 t1lstm [1,3,5,10])

#name="rnn_naive_t2lstm"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run $name training 1 t2lstm [1,3,5,10]) &
#(run $name evaluate 0 t2lstm [1,3,5,10]) &
#
#name="rnn_naive_t3lstm"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run $name training 3 t3lstm [1,3,5,10]) &
#(run $name evaluate 0 t3lstm [1,3,5,10])