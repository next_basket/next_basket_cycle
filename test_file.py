#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: test_file
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/11/5,下午10:52
#==================================

import numpy as np
import ipdb

import sys, inspect
import tensorflow as tf
# sess = tf.Session()
# x = tf.constant(np.random.uniform(0,1,(1,3)))
# y = tf.reshape(tf.tile(x,[1,5]),(5,-1))
# print(sess.run(x))
# print(sess.run(y))

x = np.random.uniform(0,1.0,(10,10))
print(np.min(x),np.max(x),np.mean(x),np.var(x))