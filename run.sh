#!/usr/bin/env bash
declare -i BATCH_SIZE=256
#subclass number 2010
#user_number 32266
#item_number 23812
run_tafeng () {
     ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/tafeng/tf_consuming -CATE_PATH ./data/tafeng/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 32266 -ITEM_NUMBER 23812 \
    -CATE_NUMBER 2010 -max_update $6 -max_cycle 50.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9 \
    -optimizer_name ${10}
}

#transaction sum 689534 mean 13.79068 density 3.68036081236156e-05
#item number 374710
#user number 50000
#100% |###################################################################################################################|
#100% |###################################################################################################################|
#category number 76
run_amazon()
{
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/amazon/amazon_review -CATE_PATH ./data/amazon/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 50000 -ITEM_NUMBER 374710 \
    -CATE_NUMBER 76 -max_update $6 -max_cycle 50.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9 \
    -optimizer_name ${10}
}

#user num 122234
#item num 384445
#cate num 6282
#transaction sum 916210 mean 7.495541338743721 density 1.949704467152316e-05
run_tianchi(){
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/tianchi/tianchi_purchase -CATE_PATH ./data/tianchi/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 122234 -ITEM_NUMBER 384445 \
    -CATE_NUMBER 6282 -max_update $6 -max_cycle 50.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9  \
    -optimizer_name ${10}
}

#transaction sum 1104494 mean 22.08988 density 0.0002000749945656112
#user 50000
#item 110408
#category 51
run_jingdong(){
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/jingdong/jd_purchase -CATE_PATH ./data/jingdong/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 50000 -ITEM_NUMBER 110408 \
    -CATE_NUMBER 51 -max_update $6 -max_cycle 50.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9  \
    -optimizer_name ${10}
}



#name="tafeng_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name training 0 ctlstm [1,3,5,10] 1000000 100 100 0.0 adagrad) &
#
#name="tafeng_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &
##
#name="tafeng_mf_bpr"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &
#
##
##name="tafeng_popular"
##/bin/rm -r ./log/$name*
##/bin/rm -r ./saved_model/$name*
##(run_tafeng $name popular 0 nlstm [1,3,5,10] 1000000 50 50  0.0 adagrad) &
##
##name="tianchi_popular"
##/bin/rm -r ./log/$name*
##/bin/rm -r ./saved_model/$name*
##(run_tafeng $name popular 0 nlstm [1,3,5,10] 1000000 50 50  0.0 adagrad) &
#
##
#name="tianchi_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tianchi $name training 2 ctlstm [1,3,5,10] 10000000 100 100 0.0 adagrad) &
#
#name="tianchi_mf_bpr"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tianchi $name training 3 nlstm [1,3,5,10] 10000000 50 50 0.0 adagrad) &
##
#name="tianchi_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tianchi $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &

name="amazon_rnn_time_attention_sum"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_amazon $name training 3 ctlstm [1,3,5,10] 10000000 100 100 0.0 adagrad) &

name="amazon_mf_bpr"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_amazon $name training 0 nlstm [1,3,5,10] 10000000 50 50 0.0 adagrad) &
#name="amazon_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name popular 3 nlstm [1,3,5,10] 10000000 50 50 0.0 adagrad) &

name="amazon_gru4rec"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_amazon $name training 1 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &
#
#name="jingdong_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name training 1 ctlstm [1,3,5,10] 10000000 100 100 0.0 adagrad) &
##
#name="jingdong_mf_bpr"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name training 3 nlstm [1,3,5,10] 10000000 50 50 0.0 adagrad) &
#
#name="jingdong_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name training 0 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &
##
##

#name="jingdong_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name popular 3 nlstm [1,3,5,10] 10000000 50 50 0.0 adagrad) &

#name="amazon_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name training 0 nlstm [1,3,5,10] 1000000 50 50 0.0 adagrad) &