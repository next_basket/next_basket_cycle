#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: metrics
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/17,上午7:06
#==================================

import numpy as np
import ipdb
import math

def hit(item_id,item_score,n=[5,10,20]):
    ranking = sorted(zip(item_score,range(len(item_score))),key=lambda x:x[0],reverse=True)
    ranking = [item[1] for item in ranking][:max(n)]
    res = []
    for item in n:
        if item_id in ranking[:item]:res.append(1)
        else: res.append(0)
    return res

def ndcg(item_id,item_score,n = [5,10,20]):
    ranking = sorted(zip(item_score,range(len(item_score))),key=lambda x:x[0],reverse=True)
    ranking = [item[1] for item in ranking][:max(n)]
    res = []
    position = np.argwhere(np.asarray(ranking)==item_id)
    if len(position)!=0:
        for item in n:
            if position[0]+1<=item:res.append(1/math.log(position[0]+2,2))
            else: res.append(0.0)
        return res
    else:
        return [0.0]*len(n)




