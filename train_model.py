#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: train_model
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午10:43
#==================================

import numpy as np
import ipdb
from arguments import parse_arguments,initialize
import utils
import os
import train
from train import *
from function_approximation import *
import tensorflow as tf
if __name__ == '__main__':
    import time
    np.random.seed(int(time.time()))
    config = []|parse_arguments|initialize
    os.environ["CUDA_VISIBLE_DEVICES"] = str(config.GPU_index)

    running = {"rnn_naive_rnn_lstm":rnn_user,
               "rnn_naive_rnn_without_user_lstm":navie_rnn,
               "rnn_naive_rnn_ctlstm":rnn_user,
               "rnn_native_rnn_without_user_lstm":navie_rnn,
               "rnn_naive_t1lstm":rnn_user,
               "rnn_naive_t2lstm":rnn_user,
               "rnn_naive_t3lstm":rnn_user,
               "rnn_naive_new_test":rnn_user,
               "rnn_naive_rnn_lstm_without_rnn_feature":rnn_user,
               "rnn_naive_rnn_lstm_without_user_feature":rnn_user,
               "rnn_user_atten":rnn_user_attention,
               "rnn_time_attention":rnn_time_attention,
               "rnn_time_attention_sum":rnn_time_attention,
               "rnn_user_lstm_sum":rnn_user,
               "mf_bpr":bpr_mf,
               "rnn_user_lstm_test_training":rnn_user,
               "amazon_mf_bpr":bpr_mf,
               "amazon_rnn_time_attention_sum":rnn_time_attention,
               "tianchi_rnn_time_attention_sum":rnn_time_attention,
               "tianchi_mf_bpr":bpr_mf,
               "tafeng_rnn_time_attention_sum":rnn_time_attention,
               "tafeng_mf_bpr":bpr_mf,
               "tafeng_popular":bpr_mf,
               "tianchi_popular":bpr_mf,
               "amazon_popular":bpr_mf,
               "jingdong_popular":bpr_mf,
               "jingdong_mf_bpr":bpr_mf,
               "jingdong_rnn_time_attention_sum":rnn_time_attention,
               "debug_rnn_time_attention_sum":rnn_time_attention,
               "jingdong_gru4rec":rnn_user,
               "amazon_gru4rec":rnn_user,
               "tianchi_gru4rec":rnn_user,
               "tafeng_gru4rec":rnn_user}

    if str(config.optimizer_name).__contains__("adam"):
        config.optimizer_name = tf.train.AdamOptimizer
    elif str(config.optimizer_name).__contains__("adagrad"):
        config.optimizer_name = tf.train.AdagradOptimizer
    elif str(config.optimizer_name).__contains__("sgd"):
        config.optimizer_name = tf.train.GradientDescentOptimizer
    name = ""
    for key in running.keys():
        if config.model.__contains__(key):
            name = key

    config.function_approximation = running[name]
    if config.task == "training":
        training(config).train()
    elif config.task == "evaluate":
        training(config).prepare_evaluate()
    elif config.task == "popular":
        training_popular(config).prepare_evaluate()

    # if config.task == 'training':
    #     if config.model=="rnn_cnn":
    #         train_cnn(config).train()
    #     elif config.model.__contains__("mf"):
    #         train_bpr_mf(config).train()
    #     else:
    #         training(config).train()
    # if config.task == 'evaluate':
    #     if config.model=="rnn_cnn":
    #         train_cnn(config).prepare_evaluate()
    #     elif config.model=="mf":
    #         train_bpr_mf(config).prepare_evaluate()
    #     elif config.model == "popular":
    #         training_popular(config).prepare_evaluate()
    #     else:
    #         training(config).prepare_evaluate()

