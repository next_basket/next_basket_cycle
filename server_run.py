#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: server_run
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/25,下午11:20
#==================================

import paramiko


def sshclient_execmd(hostname, port, username, password, execmd):
    paramiko.util.log_to_file("paramiko.log")
    s = paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(hostname=hostname, port=port, username=username, password=password)
    stdin, stdout, stderr = s.exec_command(execmd)
    stdin.write("Y")  # Generally speaking, the first connection, need a simple interaction.
    print()
    stdout.read()
    s.close()


def main():
    hostname = '166.111.71.17'
    port = 22
    username = 'zoulixin'
    password = 'zoulixin'
    execmd = "cd ~/PyCharmProject/next_basket_cycle/run.sh 2>&1 &"
    sshclient_execmd(hostname, port, username, password, execmd)

if __name__ == "__main__":
    main()
