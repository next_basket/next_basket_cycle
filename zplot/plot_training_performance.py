#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: plot_training_performance
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/11,4:59 PM
#==================================

import os

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import copy as cp
import numpy as np
import ipdb
from matplotlib import pyplot as plt
from scipy import optimize
import brewer2mpl

# brewer2mpl.get_map args: set name set type number of colors
bmap = brewer2mpl.get_map('Set2','qualitative', 7)
colors = bmap.hex_colors
import matplotlib as mpl
import matplotlib.font_manager as fm
# mpl.rcParams['axes.color_cycle'] = colors
font = fm.FontProperties(size='medium')
font.set_weight('bold')
new_font = fm.FontProperties(size='x-small')
# adding breakpoint with ipdb.set_trace()
import numpy as np
from scipy import interpolate

root_path = "/home/zoulixin/PyCharmProject/next_basket_cycle/log/"

files = ["tafeng_mf_bpr_training",
         "tafeng_rnn_time_attention_sum_training",
         "tianchi_mf_bpr_training",
         "tianchi_rnn_time_attention_sum_training",
         "amazon_mf_bpr_training",
         "amazon_rnn_time_attention_sum_training",
         "jingdong_mf_bpr_training",
         "jingdong_rnn_time_attention_sum_training"]


# print(os.listdir(root_path))
for i,t in enumerate(files):
    for j in os.listdir(root_path):
        if j.__contains__(t):
            files[i] = j

files = [os.path.join(root_path,item) for item in files]
def errorfill(x, y, yerr, alpha_fill=0.2, label =""):
    x = np.asarray(x)
    y = np.asarray(y)
    yerr = np.asarray(yerr)
    # ipdb.set_trace()
    if np.isscalar(yerr) or len(yerr) == len(y):
        # print(yerr)
        ymin = y - yerr
        ymax = y + yerr
    elif len(yerr) == 2:
        ymin, ymax = yerr
    plt.plot(x, y,label=label)
    plt.fill_between(x, ymax, ymin, alpha=alpha_fill)

def smooth(x):
    return signal.savgol_filter(np.asarray(x), 15,5)

def chunk_list(data,chunk_step = 100):
    return [data[x:x+chunk_step] for x in range(0, len(data), chunk_step)]

dataset_num = 4
metrics_num = 2
smooth_num = 20
sample_num = int(len(files)/dataset_num)

# print(files)

def read_metrics(path,num=0):
    with open(path,"r") as f:
        res = {}
        for line in f.readlines():
            if line.__contains__("metrics,hit,ndcg"):
                _,train_num,_,metrics = line.strip("\n").split("\t")
                temp = eval(metrics)
                res[int(train_num)] = temp
    x = list(sorted(res.keys()))
    y = [res[i][num][0]*1.0 for i in x]
    print("metrics", np.mean([res[i][num][0] for i in x])*1.0)
    print("metrics",np.mean([res[i][num][1] for i in x])*1.0)
    print("#"*4)
    x = [np.mean(item)/1000.0 for item in chunk_list(x,smooth_num)]
    n_y = [np.mean(item) for item in chunk_list(y,smooth_num)]
    var_y = [np.sqrt(np.var(item))/3 for item in chunk_list(y,smooth_num)]
    return x,n_y,var_y

metrics_label = ["hit@5","ndcg@5"]
data_label = ["Tafeng","Taobao","Amazon","Jingdong"]
sample_label = ["BPR","LSDEM"]
popular_performance = [0.01153654574986464,0.008500402154322745,
                       0.0030423224008757337,0.002071050497317847,
                       0.0127247956403269754,0.012167895374418294,
                       0.0127247956403269754,0.012167895374418294]
fig = plt.figure(1, figsize=(15,7), tight_layout={'pad': 0.2, 'w_pad': 2.0, 'h_pad': 1.0})
for i in range(dataset_num*metrics_num):
    plt.subplot2grid((metrics_num,dataset_num),(int(i/dataset_num),int(i%dataset_num)))
    n_metric = i%metrics_num
    n_data = int(i/metrics_num)
    for j in range(sample_num):
        x,y,var_y = read_metrics(files[j+n_data*sample_num],n_metric)
        # print(x,y,var_y)
        # print(j,n_data,sample_num)
        # print(j + n_data * sample_num)
        # plt.plot([i/5000 for i in x],[item[n_metric][0] for item in y])
        errorfill(x,y,var_y,alpha_fill=0.4,label=sample_label[j])
    # x = range(0, 1001, 200)
    # y = [popular_performance[n_metric+n_data*sample_num] for item in x]
    # plt.plot(x,y,color='black',linestyle='--',label = 'pop')
    plt.title("Performance on " + data_label[n_data])
    plt.xlabel("iteration")
    plt.ylabel(metrics_label[n_metric])
    plt.legend(loc=4)

plt.show()
fig.savefig('./training.png', dpi=fig.dpi)
fig.savefig('./training.pdf', dpi=fig.dpi)
fig.savefig('./training.eps', dpi=fig.dpi)
print("finish")




