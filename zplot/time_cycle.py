#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: time_cycle
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/13,7:03 PM
#==================================

import numpy as np
import ipdb


import os

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import copy as cp
import numpy as np
import ipdb
from matplotlib import pyplot as plt
from scipy import optimize
import brewer2mpl

# brewer2mpl.get_map args: set name set type number of colors
bmap = brewer2mpl.get_map('Set2','qualitative', 7)
colors = bmap.hex_colors
import matplotlib as mpl
import matplotlib.font_manager as fm
# mpl.rcParams['axes.color_cycle'] = colors
font = fm.FontProperties(size='medium')
font.set_weight('bold')
new_font = fm.FontProperties(size='x-small')
# adding breakpoint with ipdb.set_trace()
import numpy as np
from scipy import interpolate


import matplotlib.pyplot as plt
import numpy as np


def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Arguments:
        data       : A 2D numpy array of shape (N,M)
        row_labels : A list or array of length N with the labels
                     for the rows
        col_labels : A list or array of length M with the labels
                     for the columns
    Optional arguments:
        ax         : A matplotlib.axes.Axes instance to which the heatmap
                     is plotted. If not provided, use current axes or
                     create a new one.
        cbar_kw    : A dictionary with arguments to
                     :meth:`matplotlib.Figure.colorbar`.
        cbarlabel  : The label for the colorbar
    All other arguments are directly passed on to the imshow call.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    # ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    # ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar

def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Arguments:
        im         : The AxesImage to be labeled.
    Optional arguments:
        data       : Data used to annotate. If None, the image's data is used.
        valfmt     : The format of the annotations inside the heatmap.
                     This should either use the string format method, e.g.
                     "$ {x:.2f}", or be a :class:`matplotlib.ticker.Formatter`.
        textcolors : A list or array of two color specifications. The first is
                     used for values below a threshold, the second for those
                     above.
        threshold  : Value in data units according to which the colors from
                     textcolors are applied. If None (the default) uses the
                     middle of the colormap as separation.

    Further arguments are passed on to the created text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    # for i in range(data.shape[0]):
    #     for j in range(data.shape[1]):
    #         kw.update(color=textcolors[im.norm(data[i, j]) > threshold])
    #         text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
    #         texts.append(text)
    #         print(i,j)
    return texts

###  Accent, Accent_r, Blues, Blues_r, BrBG, BrBG_r,
# BuGn, BuGn_r, BuPu, BuPu_r, CMRmap, CMRmap_r, Dark2,
# Dark2_r, GnBu, GnBu_r, Greens, Greens_r, Greys, Greys_r,
# OrRd, OrRd_r, Oranges, Oranges_r, PRGn, PRGn_r,
# Paired, Paired_r, Pastel1, Pastel1_r, Pastel2, Pastel2_r,
# PiYG, PiYG_r, PuBu, PuBuGn, PuBuGn_r, PuBu_r, PuOr, PuOr_r,
# PuRd, PuRd_r, Purples, Purples_r, RdBu, RdBu_r, RdGy, RdGy_r,
# RdPu, RdPu_r, RdYlBu, RdYlBu_r, RdYlGn, RdYlGn_r, Reds, Reds_r,
# Set1, Set1_r, Set2, Set2_r, Set3, Set3_r, Spectral, Spectral_r,
# Wistia, Wistia_r, YlGn, YlGnBu, YlGnBu_r, YlGn_r, YlOrBr, YlOrBr_r,
# YlOrRd, YlOrRd_r, afmhot, afmhot_r, autumn, autumn_r, binary,
# binary_r, bone, bone_r, brg, brg_r, bwr, bwr_r, cividis, cividis_r,
# cool, cool_r, coolwarm, coolwarm_r, copper, copper_r, cubehelix,
# cubehelix_r, flag, flag_r, gist_earth, gist_earth_r, gist_gray,
# gist_gray_r, gist_heat, gist_heat_r, gist_ncar, gist_ncar_r,
# gist_rainbow, gist_rainbow_r, gist_stern, gist_stern_r,
# gist_yarg, gist_yarg_r, gnuplot, gnuplot2, gnuplot2_r,
# gnuplot_r, gray, gray_r, hot, hot_r, hsv, hsv_r, inferno,
# inferno_r, jet, jet_r, magma, magma_r, nipy_spectral,
# nipy_spectral_r, ocean, ocean_r, pink, pink_r, plasma,
# plasma_r, prism, prism_r, rainbow, rainbow_r, seismic,
# seismic_r, spring, spring_r, summer, summer_r, tab10,
# tab10_r, tab20, tab20_r, tab20b, tab20b_r, tab20c, tab20c_r,
# terrain, terrain_r, viridis, viridis_r, winter, winter_r

file_name = "cycle_debug_amazon_rnn_time_attention_sum.txt"
cate_path = "../data/amazon/category_info"
def load_category_label(path):
    with open(path,'r') as f:
        res = {}
        line = f.readline()
        temp = line.strip("\n").split("\t")
        key = [item for i,item in enumerate(temp) if i%2==0][:-1]
        value = [item for i,item in enumerate(temp) if i%2==1]
        for i,item in enumerate(key):
            res[int(item)] = value[i]
    value = [res[i+1] for i in range(len(res))]
    return res,value



def load_matrix():
    a = []
    with open("./"+file_name,"r") as f:
        for line in f.readlines():
            a.append(np.asarray([np.exp(float(item)-173.0) for item in line.strip("\n").strip("\t").split("\t")]))
    return np.asarray(a)

os.system("mv ../"+file_name+" ./")
a = load_matrix()
fig = plt.figure(1, figsize=(20,20), tight_layout={'pad': 0.2, 'w_pad': 2.0, 'h_pad': 1.0})
temp = a
category_dict,category = load_category_label(cate_path)
x = a
cycle_time = [a[i,i] for i in range(a.shape[0])]
cate_item = []
perf = []
for i,item in enumerate(category):
    if not len(item)<3:
        cate_item.append(item)
        perf.append(cycle_time[i])
y_pos = np.arange(len(cate_item))
print(y_pos[:30])
print(perf[:30])

plt.barh(y_pos, perf)
plt.yticks(y_pos,cate_item)
plt.xlabel('re-purchase time cycle')
plt.show()

fig.savefig('./time-cycle.png', dpi=fig.dpi)
fig.savefig('./time-cycle.eps', dpi=fig.dpi)
fig.savefig('./time-cycle.pdf', dpi=fig.dpi)