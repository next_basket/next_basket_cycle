#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: pretraining
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午10:27
#==================================

import numpy as np
import ipdb
from datasource.base import datasource
from utils.zlog import log
from metrics import hit,ndcg
import utils
import os
import time
import tensorflow as tf
from function_approximation import *

MAX_MEMORY = 3000

class basic(object):
    def __init__(self,config):
        self.config = config

    def save_agent(self,agent,save_time):
        log.info("SAVE AGENT",save_time)
        utils.save_model(agent["saver"],
                        utils.create_saved_path(save_time,self.config.saved_model_path,
                                               self.config.model),
                        agent["sess"],
                        agent["model"].global_step)

    def load_model(self,agent,save_time):
        path = os.path.join(self.config.saved_model_path,self.config.model+"_"+str(save_time))
        agent['saver'].restore(agent['sess'],tf.train.latest_checkpoint(path))
        pass

class training(basic):
    def __init__(self,config):
        self.config = config
        self.init_training()
        self.memory = []

    def init_training(self):
        self.data = datasource(self.config)

    def create_model(self):
        self.model = self.config.function_approximation.create_model(self.config,"target",True,"default")
        if self.config.restore_model:
            self.load_model(self.model, self.re_read_saved_model()[-1])
            # loss = self.model["model"].save_d_matrix(self.model["sess"])
            ipdb.set_trace()

    def train(self):
        self.create_model()
        self.train_performance = 200
        self.update_time = 0
        self.not_update = 0
        update = 0
        for item in self.data.get_training_data():
            self.memory.append(item)
            update+=1
            if len(self.memory)>MAX_MEMORY+self.config.BATCH and update%self.config.BATCH==0:
                self.memory = self.memory[self.config.BATCH:]
                self.update_model()
            # if self.not_update==100000000000:
            #     break
            if self.update_time == self.config.max_update:
                return

    def update_model(self):
        batch = [self.memory[item] for item in np.random.choice(len(self.memory),(self.config.BATCH,))]
        uid,item_seq,positive_item,negative_item,label = (list(item) for item in zip(*batch))
        item_seq,target_index,time_t = self.convert_item_seq2matrix(item_seq)
        data = {"uid":uid,
                "purchased_items":item_seq,
                "target_index":target_index,
                "positive_sample":[ii[0] for ii in positive_item],
                "negative_sample":[ii[0] for ii in negative_item],
                "labels":label,
                "purchased_time":time_t}
        # loss = self.model["model"].debug_model(self.model["sess"], data)
        loss = self.model["model"].optimize_model(self.model["sess"], data)
        self.update_time += 1
        log.info(str(self.update_time),"loss",loss)
        self.not_update+=1
        if self.train_performance>loss:
            self.not_update = 0
            self.train_performance = loss
        if self.update_time%1000==0:
            self.save_agent(self.model,self.update_time)
            log.info("saved_model_num", self.update_time, "metrics,hit,ndcg", self.train_eval())
            try:
                self.model["model"].save_d_matrix(self.model["sess"])
            except:
                log.info("no d matrix")

    def re_read_saved_model(self):
        saved_model = os.listdir(os.path.abspath(self.config.saved_model_path))
        saved_model = [int(item.split("_")[-1]) for item in saved_model if item.__contains__(self.config.model)]
        saved_model.sort()
        return saved_model

    def prepare_evaluate(self):
        self.create_model()
        samples = []
        not_new_total = 0
        not_new = 0
        while True:
            for num in self.re_read_saved_model():
                if num in samples:
                    not_new +=1
                    continue
                else:
                    not_new = 0
                    not_new_total = 0
                    try:
                    # if True:
                        samples.append(num)
                        log.info("saved_model_num", num, "metrics,hit,ndcg", self.evaluate(num))
                        # samples.append(num)
                        # print(samples)
                    except KeyboardInterrupt:
                         return
                    except FileExistsError:
                        samples.append(num)
                    except FileNotFoundError:
                        samples.append(num)
                    except:
                        time.sleep(1)
            if not_new!=0:
                not_new_total+=1
                time.sleep(1)
                # print(not_new)
                # print(not_new_total)
            if not_new_total >= 10 and len(samples)>=800:
                return

    def train_eval(self):
        hits = []
        ndcgs = []
        for item in self.data.get_evaluate_data():
            uid, seq, ground_truth = item
            item_seq, target_index, time_t = self.convert_item_seq2matrix([seq])
            data = {"uid": [uid], "purchased_items": item_seq, "target_index": [(len(item_seq) - 1, 0)],
                    "purchased_time": time_t}
            # ipdb.set_trace()
            # all_score = self.model["model"].debug_model(self.model["sess"],data)
            all_score = self.model["model"].predict_all_score(self.model["sess"], data)
            hits.append(hit(ground_truth[0], all_score, [5, 10, 15, 20, 25]))
            ndcgs.append(ndcg(ground_truth[0], all_score, [5, 10, 15, 20, 25]))
            # print(hits)
            # print(ndcgs)
        hits = zip(*hits)
        ndcgs = zip(*ndcgs)
        f = lambda metrics: [np.mean(item) for item in metrics]
        return f(hits), f(ndcgs)


    def evaluate(self,model_num):
        self.load_model(self.model, model_num)
        hits = []
        ndcgs = []
        for item in self.data.get_evaluate_data():
            uid,seq,ground_truth = item
            item_seq, target_index,time_t = self.convert_item_seq2matrix([seq])
            data = {"uid":[uid],"purchased_items":item_seq,"target_index":[(len(item_seq)-1,0)],"purchased_time":time_t}
            # ipdb.set_trace()
            # all_score = self.model["model"].debug_model(self.model["sess"],data)
            all_score = self.model["model"].predict_all_score(self.model["sess"],data)
            hits.append(hit(ground_truth[0],all_score,[5,10,15,20,25]))
            ndcgs.append(ndcg(ground_truth[0],all_score,[5,10,15,20,25]))
            # print(hits)
            # print(ndcgs)
        hits = zip(*hits)
        ndcgs = zip(*ndcgs)
        f = lambda metrics:[np.mean(item) for item in metrics]
        return f(hits),f(ndcgs)

    def convert_item_seq2matrix(self,item_seq):
        max_length = max([len(item) for item in item_seq])
        matrix = np.zeros((max_length,len(item_seq),1))
        time_t = np.zeros((max_length,len(item_seq),1))
        for x,xx in enumerate(item_seq):
            if len(xx)>0:
                current_t = xx[0][1]
                for y,yy in enumerate(xx):
                    matrix[y,x,0] = yy[0]
                    time_t[y,x,0] = yy[1]-current_t
                    current_t = yy[1]
            else:continue
        target_index = list(zip([len(i) - 1 for i in item_seq], range(len(item_seq))))
        return matrix,target_index,time_t

class training_popular(training):
    def prepare_evaluate(self):
        hits = []
        ndcgs = []
        for i,item in enumerate(self.data.get_full_evaluate_data()):
            uid,seq,ground_truth = item
            all_score = self.data.get_popular_vector()
            hits.append(hit(ground_truth[0],all_score,[5,10,15,20,25]))
            ndcgs.append(ndcg(ground_truth[0],all_score,[5,10,15,20,25]))
            if i%100==0:
                t_hits = zip(*hits)
                t_ndcgs = zip(*ndcgs)
                f = lambda metrics: [np.mean(item) for item in metrics]
                t_hits, t_ndcgs = f(t_hits), f(t_ndcgs)
                log.info(str(i),"metrics,hit", t_hits, "ndcgs", t_ndcgs)
        t_hits = zip(*hits)
        t_ndcgs = zip(*ndcgs)
        f = lambda metrics: [np.mean(item) for item in metrics]
        t_hits, t_ndcgs = f(t_hits), f(t_ndcgs)
        log.info(str(i), "metrics,hit", t_hits, "ndcgs", t_ndcgs)


