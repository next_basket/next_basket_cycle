#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: train_hawkes_process
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:2018/12/27,7:46 PM
#==================================

import numpy as np
import ipdb
from arguments import parse_arguments,initialize
import utils
import os
import train
from train import *
from function_approximation import *
import tensorflow as tf
if __name__ == '__main__':
    import time
    np.random.seed(int(time.time()))
    config = []|parse_arguments|initialize
    os.environ["CUDA_VISIBLE_DEVICES"] = str(config.GPU_index)
    running = {"tafeng_hawkes_cnn_rnn_attentive":hawkes_cnn_rnn_attentive_model}


    if str(config.optimizer_name).__contains__("adam"):
        config.optimizer_name = tf.train.AdamOptimizer
    elif str(config.optimizer_name).__contains__("adagrad"):
        config.optimizer_name = tf.train.AdagradOptimizer
    elif str(config.optimizer_name).__contains__("sgd"):
        config.optimizer_name = tf.train.GradientDescentOptimizer

    name = ""
    for key in running.keys():
        if config.model.__contains__(key):
            name = key

    config.function_approximation = running[name]

    training_hawkes(config).train()