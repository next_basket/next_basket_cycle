#!/usr/bin/env bash
declare -i BATCH_SIZE=256
#subclass number 2010
#user_number 32266
#item_number 23812
run_tafeng () {
     ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/tafeng/tf_consuming -CATE_PATH ./data/tafeng/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 32266 -ITEM_NUMBER 23812 \
    -CATE_NUMBER 2010 -max_update $6 -max_cycle 10.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9
}

#32282
run_amazon()
{
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/amazon/amazon_review -CATE_PATH ./data/amazon/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 5830 -ITEM_NUMBER 32282 \
    -CATE_NUMBER 56 -max_update $6 -max_cycle 10.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9
}

#user num 117840
#item num 79936
#cate num 3399
run_tianchi(){
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/tianchi/tianchi_purchase -CATE_PATH ./data/tianchi/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 117840 -ITEM_NUMBER 79936 \
    -CATE_NUMBER 3399 -max_update $6 -max_cycle 10.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9
}

#user 600000
#item 112797
#category 51
run_jingdong(){
    ~/pyenv3/bin/python3 ./train_model.py -log_path ./log/ -model $1 -task $2 \
    -GPU_index $3 -DATA_PATH ./data/jingdong/jd_purchase -CATE_PATH ./data/jingdong/category \
    -CELL_TYPE $4 -window_size $5 -BATCH $BATCH_SIZE -USER_NUMBER 600000 -ITEM_NUMBER 112797 \
    -CATE_NUMBER 51 -max_update $6 -max_cycle 10.0 -LATENT_FACTOR $7 -RNN_HIDDEN $8 -regularizer $9
}



#name="tafeng_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name training 0 ctlstm [1,3,5,10] 1000000 50 50 0.1) &
#
#name="tafeng_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.00) &
#
name="tafeng_mf_bpr"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_tafeng $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.00) &
#
#
#name="tafeng_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name popular 0 nlstm [1,3,5,10] 1000000 50 50 0.00)
#
#name="tianchi_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tafeng $name popular 0 nlstm [1,3,5,10] 1000000 50 50 0.00)

#
#name="tianchi_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tianchi $name training 2 ctlstm [1,3,5,10] 10000000 50 50 0.1) &
#
name="tianchi_mf_bpr"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_tianchi $name training 3 nlstm [1,3,5,10] 10000000 50 50 0.00) &

#name="tianchi_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_tianchi $name training 3 nlstm [1,3,5,10] 1000000 50 50 0.00) &

#name="amazon_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name training 2 ctlstm [1,3,5,10] 10000000 50 50 0.1) &
#
name="amazon_mf_bpr"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_amazon $name training 3 nlstm [1,3,5,10] 10000000 50 50 0.00) &
#name="amazon_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name popular 3 nlstm [1,3,5,10] 10000000 50 50 0.00)

#name="amazon_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name training 0 nlstm [1,3,5,10] 1000000 50 50 0.00) &
#
#name="jingdong_rnn_time_attention_sum"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name training 1 ctlstm [1,3,5,10] 10000000 50 50 0.00) &
#
name="jingdong_mf_bpr"
/bin/rm -r ./log/$name*
/bin/rm -r ./saved_model/$name*
(run_jingdong $name training 3 nlstm [1,3,5,10] 10000000 50 50 0.00) &
#
#name="jingdong_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name training 0 nlstm [1,3,5,10] 1000000 50 50 0.00) &
#
#

#name="jingdong_popular"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_jingdong $name popular 3 nlstm [1,3,5,10] 10000000 50 50 0.00)

#name="amazon_gru4rec"
#/bin/rm -r ./log/$name*
#/bin/rm -r ./saved_model/$name*
#(run_amazon $name training 0 nlstm [1,3,5,10] 1000000 50 50 0.00) &